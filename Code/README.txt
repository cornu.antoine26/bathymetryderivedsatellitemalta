README CODE

# Implementation of Bathymetry models on pocket beaches in Malta 
for sediment monitoring from satellite image #

This work is used to predict the bathymetry of shallow waters 
from satellite images and to follow the marine topology in time. 
It allows model creation of Machine Learning, Random Forests Regressor, 
Gradient Boosting Regressor, their backup and use to predict. 
These codes were made with Worldview3 and Sentinel-2 data, 
suitable for a multi-band image (Worldview3) or for bands (Sentinel-2).


## Different features ##

This work has 5 Main python files, described quickly below and
the usage explanations, settings in the files.

-  MainCreateModelLinearModel.py; allows you to create a linear regression 
model and evaluate its accuracy.

-  MainCreateModelRF.py; allows the creation of a Machine Learning model 
and the evaluation of its accuracy. The available Machine Learning 
algorithms are Random Forest Regressor and Gradient Boosting Regressor, 
from the python scikit-sklearn library. 
Other algorithms from this library can be added to be tested. 
The addition is done in the MachineLearningRegressor.py Function file 
in the dictionary line 15. Then Specification of the assigned name 
in the Main settings.

-  Test_Log-Ratio.py; used to test the robustness of linear models 
of the ratio-log transformed band method.

-  Test_ML.py; is used to test the robustness of machine learning models.

-  UsingModel.py; allows you to use templates on images for 
allow the prediction of bathymetry.

All the parameters of the different files for their use are 
explained at the beginning of the code.


## Other files ##

The document Function contains the other .py files 
allowing the operation of the different Main.

QGIS Data Processing Guide is a guide to use if the buildVRT 
the function of gdal does not work


## Package to be installed in an environment for code operation ##

Pay attention to the packages and python versions. 
The version of python used is 3.6, and 3.7 can be used too, 
but the versions above are not. The packages used and their versions are:

-  Numpy 1.19.2 -> Allows image manipulation.

-  Matplotlib 3.3.4 -> Visualization of data as a graph.

-  Lxml 4.6.3 -> Allows you to read images in the XML to search for data for the ratio-log band method.

-  Scikit-learn 0.24.2 /scikit-image 0.17.2 -> The different methods of Machine Learning, training, evaluation.

-  Imageio 2.9.0 -> Image Backup.

-  Gdal 3.0.2 -> Loading/Saving images in a projection system, GIS package.

-  Joblib 1.0.1 -> Save/Load created templates.

In python 3.6 if you use anaconda the different packages will install 
automatically under the correct version, in 3.7 you will have 
to downgrade a version of gdal to make it work. 
The package to be careful with this version is the gdal package, 
the other packages may have another version.
