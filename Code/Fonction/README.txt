README FONCTION

The files are:

-  ChangeBathymetry.py; Modifies the bathymetry map to specified areas. 
	Specific to location and work data. If the data is different from the data 
	originally used, set False to the DeletionBigBump variable in the Main.

-  ConversionRadRef.py; Calculates the top-of-atmosphere reflectances of 
	Worldview-3 images for the ratio-log band method, or Machine Learning 
	methods if False is set to RadData in the Main MainCreateModelRF.py.

-  Kmean_PlantSand.py; Allows the implementation of a Machine Learning 
	K-Means algorithm to classify the sand/no-sand(plant) marine soil.

-  LinearModel.py; Allows the implementation of the linear regression model.

-  LoadDataGdal.py; Allows data loading and unification if different 
	resolution (satellite image, DEM).

-  LowFilter.py; Allows application of kernel filter on data.

-  MachineLearningRegressor.py; Enables the implementation and 
	evaluation of Machine Learning models for bathymetry prediction. 

-  Masquing.py; allows the creation of different masks for the different 
	classifications proposed.

-  Plot.py; allows you to plot different values (x y, one slice, etc.).

-  ProcessingData.py; Allows image processing, such as radiance to 
	reflectance conversions.