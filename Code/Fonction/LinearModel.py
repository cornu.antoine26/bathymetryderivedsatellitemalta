# -*- coding: utf-8 -*-
"""
Created on Fri May 27 15:09:44 2022

@author: Antoine Cornu student of ENSG France
"""

from sklearn.linear_model import LinearRegression  
from sklearn.metrics import mean_squared_error, r2_score

import numpy as np
import Fonction.Plot as plot
import imageio as iio

def convertisseur(x,y):
    """
    Allows the conversion of two matrices of square dim 2 or rectangle to a 
    matrix colone x and y have the same dimensions
    
    Parameters
    ----------
    x : array
        matrice of x
    y : array
        matrice of y

    Returns
    -------
    array, array
        the two colones matrices

    """
    l,c = x.shape
    xt = []
    yt = []
    for i in range(l):
        for j in range(c):
            if (not(np.isnan(y[i,j]))) and (not(np.isnan(x[i,j]))):
                xt.append([x[i,j]])
                yt.append([y[i,j]])
  
    return np.array(xt),np.array(yt)


def convertisseur2(bands):
    """
    Allows the conversion of 3 matrices of square dim 2 or rectangle to a 
    matrix colone 
    
    Parameters
    ----------
    bands: array
        matrice of x

    Returns
    -------
    ndarray
        the matrice colone and the coordinates points

    """
    l,c = bands[0].shape
    coo = []
    b1 = []

    for i in range(l):
        for j in range(c):
            three = []
            for x in bands:
                three.append(x[i,j])
            if not(np.isnan(np.amax(np.array(three)))):
                coo.append([i,j])
                b1.append(three[0])

    return np.array(b1), coo


def DataXY(T, E):
    """
    Creation of dataset for the Lineare Regression, with ln(Band_Bleu)/ln(Band_Green)

    Parameters
    ----------
    T : list of array
        List with differents bands for training
    E : list of array
        List with differents bands for evaluation

    Returns
    -------
    x : ndarray dim 2
        x dataset for trainning model.
    y : ndarray dim 2
        y dataset for trainning model.
    xe : ndarray dim 2
        x dataset for evaluation model.
    ye : ndarray dim 2
        y dataset for evaluation model.

    """
    x = T[0]
    y = T[1]

    xe = E[0]
    ye = E[1]   
            
    x,y = convertisseur(x,y)
    xe,ye = convertisseur(xe,ye)
    return x, y, xe, ye


def Dataset(BandRef):
    """
    Create Dataset for Random Forest

    Parameters
    ----------
    BandRef : list of array
        list of different reflectance bands and other arrays for the feature of the dataset.
    BandZ : array
        matrice of bathymetry, or just y value.

    Returns
    -------
    features : array
        Dataset of feature for random forest.
    labels : array
        Dataset of layer for random forest.

    """
    features, coo = convertisseur2(BandRef)
    
    return [features, coo]


def TrainLineaireModel(x,y):
    """
    Create, train the lineare model

    Parameters
    ----------
    x : colone array 
        values of x
    y : colone array 
        values of y

    Returns
    -------
    model : model LinearRegression
        The lineare model
    """
    
    model = LinearRegression()
    model.fit(x, y)
    y_new = model.predict(x)

    rmse = np.sqrt(mean_squared_error(y,y_new))
    r2 = r2_score(y,y_new)
    errors = abs(y_new - y)
    print('RMSE: ', rmse)
    print('R2: ', r2)
    print('Mean Absolute Error:', round(np.mean(errors), 2), 'meters.')
    print('Median Absolute Error:', round(np.median(errors), 2), 'meters.')
    
    coef = model.coef_
    inte = model.intercept_
    print('coefficient a :',coef[0,0],' coefficient b :',inte[0])
    
    return model


def EvalModel(model, x, y):
    """
    Model evaluation

    Parameters
    ----------
    model : model
        the lineare model.
    x : colone array
        values of x.
    y : colone array
        values of y.

    Returns
    -------
    None.

    """
    y_val = UseModel(model, x)

    error = abs(y_val-y)
    error2 = error**2
    MAE = np.nanmean(error)
    MAE2 = np.nanmean(error2)
    MedAE = np.nanmedian(error)
    rmse_2 = np.sqrt(MAE2)
    
    print('RMSE: ', rmse_2)
    print('MAE : ', MAE)
    print("MedAE : ", MedAE)
    
    return y_val


def UseModel(model, x):
    """
    Using the model for data

    Parameters
    ----------
    model : model
        the lineare model.
    x : array
        values of x.

    Returns
    -------
    colone array
        y predict
    """
    coef = model.coef_
    inte = model.intercept_
    z = coef*x+inte
    return z


def UseLinearModel(Bands_t,Bands_e, plotDia=False, titleDia='Diagram', save_predic=False, titleImg_p='Img_predic.tif', save_reel=False,  titleImg_r='Img_reel.tif', PlotCut=False, poscut = 0, titleCut='Diagram'):
    """
    The algorithm that makes the model and trains it from the training and 
    evaluation bands Bands_n = [log(rB)/log(rG),Z] 

    Parameters
    ----------
    Bands_t : list of array
        list of different bands log(rBlue)/log(rGreen), Z = Depth (Bathymetry) for the training.
    Bands_e : list of array
        list of different bands log(rBlue)/log(rGreen), Z = Depth (Bathymetry) for the evaluation.
    plotDia : boolean, optianal
        Plot the diagramme of values, The default is False.

    Returns
    -------
    model : model
        the model of the Linear Regression.

    """
    xt,yt,xe,ye = DataXY(Bands_t,Bands_e)
    # xt, yt = DelBruitLogBand(xt, yt)
    model = TrainLineaireModel(xt,yt)
    # EvalModel(model, xe, ye)
    z_p = UseModel(model, Bands_t[0])
    
    if plotDia:
        plot.PlotXY(xt, yt, title=titleDia, xlabel='log(rBlue)/log(rGreen)', ylabel='Bathymetry')
        
    if save_predic:
        iio.imsave(titleImg_p, z_p)
        
    if save_reel:
        iio.imsave(titleImg_r, Bands_t[1])
        
    if PlotCut:
        y1 = Bands_t[1][:,poscut]
        y2 = z_p[:,poscut]
        plot.PlotCut(y1,y2, title=titleCut)
        
    # iio.imsave('Img_logband.tif', Bands_t[0])
    
    return model


# def DelBruitLogBand(x, y): #Juste pour sentinel2 L3A Théia
#     l,c = y.shape
#     for i in range(l):
#         x1 = -0.005*y[i,0] + 0.945
#         x2 = -1/225*y[i,0] + 221/225
#         if x[i,0]<x1:
#             x[i,0]=x1
#         if x[i,0]>x2:
#             x[i,0]=x2
            
#     return x, y