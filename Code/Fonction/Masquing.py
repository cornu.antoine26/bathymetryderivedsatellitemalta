# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 12:19:36 2022

@author: Antoine Cornu student of ENSG France
"""
import numpy as np
import Fonction.Kmean_PlantSand as kmps
import imageio as iio

def MaskWater(Bands, BandNir, threshold = 100):
    """
    Application of masquing on bands of images to separate land and water and keeping water.

    Parameters
    ----------
    Bands : list of ndarray
        List of bands of image to want to mask.
    BandNir : ndarray of dim 2
        Band NIR of the image.
    threshold : int, optional
        The threshold to create the mask of land water in the NIR band. 
        The default is 100.

    Returns
    -------
    MasqBands : list of ndarray
        List with hidden bands.

    """
    mask = np.where(BandNir<=threshold,1,0)
    MaskBands = []
    for i in Bands:
        bandi = i*mask
        bandi = np.where(bandi==0,np.nan,bandi)
        MaskBands.append(bandi)
    
    return MaskBands


def MaskDepth(Bands, BandZ, S = 0, Depth=0):
    """
    Application of masquing data to keep data where there is data of depth

    Parameters
    ----------
    Bands : list of ndarray
        List of bands to mask.
    BandZ : ndarray
        band of depth.
    S : int, optional
        Value of separation upper. The default is 0.
    Depth : int, optional
        Value of the Depth not to exceed. The default is 0.

    Returns
    -------
    list of ndarray
        List of hidden bands, with band of depth.

    """
    BandZ = np.where(BandZ>S, np.nan, BandZ)
    if Depth != 0:
        BandZ = np.where(BandZ<Depth, np.nan, BandZ)
    mask = np.where(np.isnan(BandZ), 0, 1)
    MaskBands = []
    for i in Bands:
        bandid = i*mask
        bandid = np.where(bandid==0,np.nan,bandid)
        MaskBands.append(bandid)
    
    # MaskBands.append(BandZ)
    return MaskBands
        

def MaskPlant(Bands, BandB, rad = 202):
    """
    Creation of the mask for separeted aquatic plant and sand

    Parameters
    ----------
    Bands : list of ndarray
        List with different bands to want to apply the mask.
    BandB : ndarray
        array of the blue band
    rad : int, optional
        value of the radiation on the image Blue to cut. The default is 202.

    Returns
    -------
    Sand : list of ndarray
        List with just the sand in the band.
    Plant : list of ndarray
        List with just aquatic plant in the band.

    """
    MaskSand = np.where(BandB<=rad,0,1)
    MaskPlant = np.where(BandB>=rad,0,1)
    Sand = []
    Plant = []
    for i in Bands:
        bs = i*MaskSand
        bs = np.where(bs==0,np.nan,bs)
        bp = i*MaskPlant
        bp = np.where(bp==0,np.nan,bp)
        Sand.append(bs)
        Plant.append(bp)
    
    
    return Sand, Plant


def MaskPlantSand(Bands, pos_bleu, pos_bandZ, coorSand, saveMask = False):
    """
    Mask application to classify pixel sand, plant and other if there are any 

    Parameters
    ----------
    Bands : list of array
        List of different bands of the image.
    pos_bleu : int
        Position of the bleu band in the list.
    pos_bandZ : int
        Position of the bathymetry band in the list.
    coorSand : list of list
        List of coordinates (1 enough) of a sand pixel.
    saveMask : boolean, optional
        Save sand's mask and plant's mask. The default is False.

    Returns
    -------
    BandsSand : list of array
        list of data with sand band apply.
    BandsPlant : list of array
        list of data with plant band apply.

    """
    BandsSand = []
    BandsPlant = []
    BandsOther = []
    maskdata = np.where(np.isnan(Bands[0]),0,1)
    bands = Bands[pos_bleu]
    bandZ = Bands[pos_bandZ]
    MS, MP = kmps.MaskPlantSandByKMeans([bands], bandZ, coorSand)
    M = abs(MS+MP)
    M = np.where(M>1,1,M)
    Mnod = maskdata + M
    Mnod = np.where(Mnod!=1,0,1)
    count = np.count_nonzero(Mnod)
    for b in Bands:
        BandsSand.append(np.where((b*MS)==0,np.nan, b*MS))
        BandsPlant.append(np.where((b*MP)==0,np.nan, b*MP))
        if count != 0:
            BandsOther.append(np.where((b*Mnod)==0,np.nan,b*Mnod))
            
    if saveMask:
        iio.imsave('Mask_Sand.tif', MS)
        iio.imsave('Mask_Plant.tif', MP)
        
    return BandsSand, BandsPlant, BandsOther


def MaskPlantSand2(Bands, pos_bleu, coorSand, saveMask = False):
    """
    Mask application to classify pixel sand, plant and other if there are any 

    Parameters
    ----------
    Bands : list of array
        Liste of differente bands of the image.
    pos_bleu : int
        Position of the bleu band in the list.
    pos_bandZ : int
        Position of the bathymetry band in the list.
    coorSand : list of list
        List of coordinates (1 enough) of a sand pixel.
    saveMask : boolean, optional
        Save sand's mask and plant's mask. The default is False.

    Returns
    -------
    BandsSand : list of array
        Band with just data of sand.
    BandsPlant : list of array
        Band with just data of plant.

    """
    BandsSand = []
    BandsPlant = []
    BandsOther = []
    maskdata = np.where(np.isnan(Bands[0]),0,1)
    bands = Bands[pos_bleu]
    MS, MP = kmps.MaskPlantSandByKMeans2([bands], coorSand)
    M = abs(MS+MP)
    M = np.where(M>1,1,M)
    Mnod = maskdata + M
    Mnod = np.where(Mnod!=1,0,1)
    count = np.count_nonzero(Mnod)
    for b in Bands:
        BandsSand.append(np.where((b*MS)==0,np.nan, b*MS))
        BandsPlant.append(np.where((b*MP)==0,np.nan, b*MP))
        if count != 0:
            BandsOther.append(np.where((b*Mnod)==0,np.nan,b*Mnod))
            
    if saveMask:
        iio.imsave('Mask_Sand.tif', MS)
        iio.imsave('Mask_Plant.tif', MP)
        
    return BandsSand, BandsPlant, BandsOther


def UsingKMeans(model, Bands, coorsand, saveMask = False):
    """
    Application of mask sand/plant on data by K-Means algorithm

    Parameters
    ----------
    model : model
        K-Means algorithme.
    Bands : list of array
        List of different band of the image.
    coorsand : list of list
        There is one coordinate of a one pixel of sand.
    saveMask : boolean, optional
        Save sand's mask and plant's mask. The default is False.

    Returns
    -------
    BandsSand : list of array
        Band with just data of sand.
    BandsPlant : list of array
        Band with just data of plant.

    """
    BandsSand = []
    BandsPlant = []
    blueband = Bands[0]
    MS, MP = kmps.MaskKMeans(model, blueband, coorsand)
    for b in Bands:
        BandsSand.append(np.where((b*MS)==0,np.nan, b*MS))
        BandsPlant.append(np.where((b*MP)==0,np.nan, b*MP))
        
    if saveMask:
        iio.imsave('Mask_Sand.tif', MS)
        iio.imsave('Mask_Plant.tif', MP)
        
    return BandsSand, BandsPlant


def Masquing(bands, s2_lC1, Water = True, DataDepth = True, maxDepth = 0, minDepth = 0, Plant = True, rad = 202, threshold=100):
    """
    Application of the different masks on the input data

    Parameters
    ----------
    bands : list of ndarray
        List of the bands image.
    s2_lC1 : boolean
        For data of Sentinel2 level C1
    Water : boolean, optional
        Application of this mask. The default is True.
    DataDepth : boolean, optional
        Application of this mask. The default is True.
    maxDepth : int, optional
        Upper value to separate data of bathymetry, all data higher this value will be deleted. The default is 0. 
    minDepth : int, optional
        Value of the Depth not to exceed. The default is 0.
    Plant : boolean, optional
        Application of this mask. The default is True.
    rad : int, optional
        value of the radiation on the image Blue to cu. The default is 202.
    threshold : int, optional
        The threshold to create the mask of land water in the NIR band. The default is 100.

    Returns
    -------
    b : list of ndarray
        List of the band reflectance and band of Depth.

    """
    if s2_lC1:
        logb = 3
        bathyb = 2
        Water = False
        Plant = False
    else:
        logb = 5
        bathyb = 4
    
    b = [bands[logb], bands[bathyb]]
    if Water:
        b = MaskWater(b, bands[3], threshold)
    if DataDepth:
        b = MaskDepth([b[0]], b[1], S=maxDepth, Depth=minDepth)
    if Plant:
        bs,bp = MaskPlant(b, bands[0], rad)
        b = [bs,bp]
        
    return b