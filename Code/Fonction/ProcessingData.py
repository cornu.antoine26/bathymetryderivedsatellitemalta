# -*- coding: utf-8 -*-
"""
Created on Tue May 31 09:34:18 2022

@author: Antoine Cornu student of ENSG France
"""
import numpy as np
import Fonction.ConversionRadRef as crr
    

def ProDataRef(bands, xmlpath, data, listbands, nb_data_ref, pos_bandz):
    """
    Calculate reflectance of band each band you want

    Parameters
    ----------
    bands : list of list of ndarray
        list of  a satellite image with the band containing data of the depth.
    xmlpath : list of string
        list of path for the xml of the image.
    data : dico
        Dictionnary of value.
    listbands : list of string
        list of different band which will be converted into reflectance ex: listbands = ['B','G']
    nb_data_ref : int
        number of band to calculate reflectance TOA
    pos_bandz : int
        position of the Depth Band

    Returns
    -------
    bands : list of ndarray
        List of each reflectance band.

    """
    ref = crr.convRadToRefTOA(bands[0:nb_data_ref], xmlpath, data, listbands)
    ref.append(bands[pos_bandz])
    ref.append(bands[0])
    
    return ref


def logB1divlogB2(B1, B2):
    """
    Return list of band with log(B1)/log(B2) more

    Parameters
    ----------
    B1 : array
        band for the numerateur.
    B2 : array
        band for the denominateur.

    Returns
    -------
    array
        log bands.

    """
    return np.log(B1)/np.log(B2)


def ProDataRefLinear(bands, xmlpath, data, s2_l3A, n=1):
    """
    Manipulate the data with the starting image to directly calculate the 
    reflectance of the blue and green bands, then apply log(rB)/log(rG)

    Parameters
    ----------
    bands : list of list of ndarray
        list of a satellite image with the band containing data of the depth.
    xmlpath : list of string
        list of path for the xml of the image.
    data : dico
        Dictionnary of value.

    Returns
    -------
    bands : list of ndarray
        List of each band, plus the band of log(rB)/log(rG).

    """
    listbands = ['B','G']
    if s2_l3A:
        bandlog = np.log(n*bands[0])/np.log(n*bands[1])
        #bandlog = np.where(np.isnan(bandlog),0,bandlog)
    else:
        ref = crr.convRadToRefTOA(bands[0:2], xmlpath, data, listbands)
        # ref = crr.convRadToRrf(bands[0:2], xmlpath, data, listbands)
        
        bandlog = np.log(n*ref[0])/np.log(n*ref[1])
        #bandlog = np.where(np.isnan(bandlog),0,bandlog)
        
    bands.append(bandlog)
    return bands


def WV3ref(bands, xmlpath, data, listbands):
    """
    Calculate the reflectance of bands of Worldview 3

    Parameters
    ----------
    bands : list of array
        it's a list with bands of Worldview 3 in the order B,G,R....
    xmlpath : string
        The path of the xml of the image data (metadata).
    data : dicto
        The dictionary of different data.
    listbands : list of string
        list of a different band, you want to convert.

    Returns
    -------
    ref : list of array
        list of different reflectance bands, add the Bathymetry band in position -2, and bleu band in -1.

    """
    nb = len(listbands)
    ref = crr.convRadToRefTOA(bands[0:nb], xmlpath, data, listbands)
    ref.append(bands[4]); ref.append(bands[0])
    return ref
    

def Bands(img):
    """
    Separates the differences bands of an image

    Parameters
    ----------
    img : ndarray of dim 3
        The image of processing.

    Returns
    -------
    bands : list of ndarray
        The list of each band, there is in the image. [B,G,....]

    """
    nb_band = img.shape[0]
    bands = []
    for i in range(nb_band):
        bands.append(img[i,:,:])
    
    return bands


def TransposeImg(img):
    """
    Transpose the image of dim (0,1,2) to dim (1,2,3)

    Parameters
    ----------
    img : ndarray
        The image to transpose.

    Returns
    -------
    img : ndarray
        The transpose image.

    """
    img = img.transpose((1,2,0))
    return img


def norm(img):
    """
    Normalized the img, means = 0 and var = 1

    Parameters
    ----------
    img : ndarray
        the image.

    Returns
    -------
    ndarray
        the normalized image.

    """
    return img / np.linalg.norm(img)

