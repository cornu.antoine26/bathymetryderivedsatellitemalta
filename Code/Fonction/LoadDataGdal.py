# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 09:17:20 2022

@author: Antoine Cornu student of ENSG France
"""
import sys
from osgeo import gdal, osr
import numpy as np
import Fonction.ChangeBathymetry as cb


def Download_Data(img, bathy, band, s2, DBB, titleDBB):
    """
    Download data raster with coordinate, to create one raster with 2 data raster and converting in array the raster result

    Parameters
    ----------
    img : string or list of string
        if one image with many bands band=False, and if list with bands band=True.
    bathy : string
        The path of the bathymetry image.
    band : boolean
        if there is one paths for the image or many path for each band of the image.
    s2 : boolean
        True if the data of the image is Sentinel-2.
    DBB : boolean
        True in the specific  case, where delete bump is the bathymetry, but is manual.
    titleDBB : string
        Name of the new bathymetry band if the DBB is True.

    Returns
    -------
    img_vrt : array
        image with a different band.

    """
    gdalimg = []
    if band:
        for path in img:
            gdalimg.append(gdal.Open(path, gdal.GA_ReadOnly))
    else:
        
        imggdal = gdal.Open(img, gdal.GA_ReadOnly)
        nb_bands = imggdal.RasterCount
        imgarray = imggdal.ReadAsArray()
        for i in range(nb_bands):
            gdalimg.append(array2raster(imggdal, f'band_{i}.tiff', imgarray[i,:,:]))
    
    if s2:
        resolution = 'lowest'
    else:
        resolution = 'highest'
    
    if DBB:
        gdalbathy = gdal.Open(bathy, gdal.GA_ReadOnly)
        bathy_array = gdalbathy.ReadAsArray()
        bathy = cb.ChangeBathyArea(bathy_array)
        gdalimg.append(array2raster(gdalbathy, titleDBB, bathy))
    else:
        gdalimg.append(gdal.Open(bathy, gdal.GA_ReadOnly))
    
    vrt_options = gdal.BuildVRTOptions(resolution=resolution ,separate=True)
    my_vrt = gdal.BuildVRT("my_vrt", gdalimg, options=vrt_options)
    my_vrt.FlushCache()
    img_vrt = my_vrt.ReadAsArray()
    
    img_vrt[-1,:,:] = np.where(img_vrt[-1,:,:]<-100, np.nan, img_vrt[-1,:,:])    
    img_vrt = np.where(img_vrt==0, np.nan, img_vrt)
    
    return img_vrt


def array2raster(raster,newRasterfn,array):
    """
    Create a raster gdal thanks to an array, in relation with an second raster for the coordinate
    """
    geotransform = raster.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]
    cols = raster.RasterXSize
    rows = raster.RasterYSize

    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Float32)
    outRaster.SetGeoTransform((originX, pixelWidth, 0, originY, 0, pixelHeight))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromWkt(raster.GetProjectionRef())
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()
    
    return outRaster


def Load_imggdalmultiband(path):
    """
    Load image with multi bands on gdal

    Parameters
    ----------
    path : string
        path of the image.

    Returns
    -------
    imggdal : gdal image
        gdal image.
    imgarray : array
        array image.

    """
    imggdal = gdal.Open(path, gdal.GA_ReadOnly)
    imgarray = imggdal.ReadAsArray()
    imgarray[-1,:,:] = np.where(imgarray[-1,:,:]<-100, np.nan, imgarray[-1,:,:])    
    imgarray = np.where(imgarray==0, np.nan, imgarray)
    return imggdal, imgarray

def verif(img):
    """
    Check if the image is good for processing

    Parameters
    ----------
    img : array
        the image process.

    Returns
    -------
    None.

    """
    a = np.nanmax(img)
    if np.isnan(a):
        print('The buildvrt function of gdal does not work, start image processing on QGIS, see «Guide QGIS processing Data» and review the parameter on gdal processing parameters, over.')
        sys.exit()