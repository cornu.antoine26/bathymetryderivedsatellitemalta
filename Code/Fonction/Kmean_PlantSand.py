# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 09:37:02 2022

@author: Antoine Cornu student of ENSG France
"""

import joblib as jlib
import numpy as np
from sklearn.cluster import KMeans


def MaskPlantSandByKMeans(Bands, BandZ, coorSandPlant, minDepth=0, maxDepth=-10):
    """
    determines plant and sand masks from a Kmeans algorithm, with an additional depth parameter

    Parameters
    ----------
    Bands : list array
        List of bands to train the kmean.
    BandZ : array
        Bathymetry band.
    coorSandPlant : list of list of int
        List of coordinates that one pixel sand.
    minDepth : int, optional
        Minimum of Depth you want. The default is 0.
    maxDepth : int, optional
        Maximum of Depth you want. The default is -10.

    Returns
    -------
    maskSand : array
        Mask of Sand.
    maskPlant : array
        Mask of Plant.

    """
    l,c = Bands[0].shape
    maskSand = np.zeros((l,c))
    maskPlant = np.zeros((l,c))
    maskDepth1 = np.where(minDepth<=BandZ, 1, np.nan)
    maskDepth2 = np.where(BandZ<=maxDepth, 1, np.nan)
    maskDepth = maskDepth1*maskDepth2
    for b in Bands:
        b = b*maskDepth
    X, coor = Convertion(Bands)
    kmeans = CreateKMeans(X)
    Labels = kmeans.labels_
    X_p = X_predic(Bands, coorSandPlant)
    predict = kmeans.predict(X_p)
    Label_Sand = predict[0]
    nb_i = len(coor)
    Labelsinv = abs(Labels - 1)
    if Label_Sand == 0:
        for c in range(nb_i):
            maskPlant[coor[c][0],coor[c][1]] = Labels[c]
            maskSand[coor[c][0],coor[c][1]] = Labelsinv[c]
    else:
        for c in range(nb_i):
            maskSand[coor[c][0],coor[c][1]] = Labels[c]
            maskPlant[coor[c][0],coor[c][1]] = Labelsinv[c]
    
    # jlib.dump(kmeans, "ModelKMeans.sav")
    
    return maskSand, maskPlant


def MaskPlantSandByKMeans2(Bands, coorSandPlant):
    """
    determines plant and sand masks from a Kmeans algorithm, with an additional depth parameter

    Parameters
    ----------
    Bands : list array
        List of bands to train the kmean.
    BandZ : array
        Bathymetry band.
    coorSandPlant : list of list of int
        List of coordinates that one pixel sand.
    minDepth : int, optional
        Minimum of Depth you want. The default is 0.
    maxDepth : int, optional
        Maximum of Depth you want. The default is -10.

    Returns
    -------
    maskSand : array
        Mask of Sand.
    maskPlant : array
        Mask of Plant.

    """
    l,c = Bands[0].shape
    maskSand = np.zeros((l,c))
    maskPlant = np.zeros((l,c))
    X, coor = Convertion(Bands)
    kmeans = CreateKMeans(X)
    Labels = kmeans.labels_
    X_p = X_predic(Bands, coorSandPlant)
    predict = kmeans.predict(X_p)
    Label_Sand = predict[0]
    nb_i = len(coor)
    Labelsinv = abs(Labels - 1)
    if Label_Sand == 0:
        for c in range(nb_i):
            maskPlant[coor[c][0],coor[c][1]] = Labels[c]
            maskSand[coor[c][0],coor[c][1]] = Labelsinv[c]
    else:
        for c in range(nb_i):
            maskSand[coor[c][0],coor[c][1]] = Labels[c]
            maskPlant[coor[c][0],coor[c][1]] = Labelsinv[c]
    
    return maskSand, maskPlant


def MaskKMeans(model, bandb, coorsand):
    """
    Create mask of sand and plant, with a model of K-Means already existing

    Parameters
    ----------
    model : model
        model of K-Means.
    bandb : array
        Blue Band of the image.
    coorsand : list of list
        There is one coordinate of a one pixel of sand.

    Returns
    -------
    maskSand : array
        binary mask of sand.
    maskPlant : array
        binary mask of plant.

    """
    l,c = bandb.shape
    maskSand = np.zeros((l,c))
    maskPlant = np.zeros((l,c))
    X, coor = Convertion([bandb])
    predic = model.predict(X)
    X_p = X_predic([bandb], coorsand)
    predictp = model.predict(X_p)
    Label_Sand = predictp[0]
    nb_i = len(coor)
    predicinv = abs(predic - 1)
    if Label_Sand == 0:
        for c in range(nb_i):
            maskPlant[coor[c][0],coor[c][1]] = predic[c]
            maskSand[coor[c][0],coor[c][1]] = predicinv[c]
    else:
        for c in range(nb_i):
            maskSand[coor[c][0],coor[c][1]] = predic[c]
            maskPlant[coor[c][0],coor[c][1]] = predicinv[c]
            
    return maskSand, maskPlant


def X_predic(Bands, coorSP):
    """
    Create a dataset with one point one each type of area, by coordinate of one point in each area

    Parameters
    ----------
    Bands : list of array
        List of different bands for the KMeans.
    coorSP : list of list
        list of coordinates of one point in each type of area [coorSand, coorPlant].

    Returns
    -------
    X : array
        the dataset for the prediction.

    """
    X = []
    for coo in coorSP:
        x = []
        for ba in Bands:
            x.append(ba[coo[0],coo[1]])
        X.append(x)
    return np.array(X)     


def CreateKMeans(X, nb_cluster = 2):
    """
    Creation of K-Means algorithm

    Parameters
    ----------
    X : array
        Data for training algorithm.
    nb_cluster : int, optional
        number of cluster. The default is 2.

    Returns
    -------
    kmeans : model
        The K-Means algorithm.

    """
    kmeans = KMeans(n_clusters=nb_cluster).fit(X)
    return kmeans


def Convertion(Bands):
    """
    Allows the conversion of the matrice of square dim 2 or rectangle to a 
    matrix colone 
    
    Parameters
    ----------
    Bands : ndarray
        Bands for the K-means agorithm

    Returns
    -------
    ndarray
        the matrice colone
    list
        List of coordinates

    """
    l,c = Bands[0].shape
    t = len(Bands)
    x = []
    coor = []
    for i in range(l):
        for j in range(c):
            xx = []
            for b in Bands:
                if not(np.isnan(b[i,j])):
                    xx.append(b[i,j])
            if len(xx)==t:
                x.append(xx)
                coor.append([i,j])
    return np.array(x), coor


