# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 15:10:18 2022

@author: Antoine Cornu student of ENSG France
"""

import matplotlib.pyplot as plt
import numpy as np


def PlotReflectance(b, title = 'Diagram of log on depth', xlabel = 'reflectance log/log', ylabel = 'depth'):
    """
    Plot ratio log of 2 bands, b is a list of two array
    """
    x = np.log(b[0])/np.log(b[1])
    fig, axs = plt.subplots()
    axs.scatter(x,b[2])
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)
    
    
def PlotXY(x, y, title = 'Diagram', xlabel = '', ylabel = ''):
    """
    Plot data with x and y
    """
    fig, axs = plt.subplots()
    axs.scatter(x,y)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)


def Plothist(x, title = 'Histogramme', xlabel='gap', ylabel='number'):
    """
    Plot histogram of an array x
    """
    counts, bins = np.histogram(x)
    fig, axs = plt.subplots()
    axs.hist(bins[:-1], bins, weights=counts)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)
    

def PlotCut(y1, y2, title = 'Diagram', xlabel = 'Position', ylabel = 'Bathymetry'):
    """
    Plot two cut, y1, y2 in relation to their position, the first data in the colone array take the position 1
    """
    t = y1.shape
    x = np.array([i for i in range(t[0])])
    fig, axs = plt.subplots()
    axs.scatter(x,y1)
    axs.scatter(x,y2)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)


def PlotLog(s,p,y,title = 'Diagram', xlabel = 'ratioband', ylabel='Bathymetry (m)'):
    """
    Plot 2 things s and p by y
    """
    # x1 = np.log(s[0])/np.log(s[1])
    # x2 = np.log(p[0])/np.log(p[1])
    fig, axs = plt.subplots()
    axs.scatter(s,y,c='#1f77b4')
    axs.scatter(p,y,c='#d62728')
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)
    
    
def PlotCutOne(y1, title = 'Cut Diagram bathymetry', xlabel = 'Position', ylabel = 'bathymetry'):
    """
    Plot a cut, y1 in relation to their position, the first data in the colone array take the position 1
    """
    t = y1.shape
    x = np.array([i for i in range(t[0])])
    fig, axs = plt.subplots()
    axs.scatter(x,y1)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)