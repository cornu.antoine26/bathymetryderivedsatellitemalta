# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 13:45:09 2022

@author: Antoine Cornu student of ENSG France
"""
import sys
import numpy as np
import imageio as iio
import Fonction.Plot as plot
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_squared_error

DicoML = {'Random Forest Regressor': RandomForestRegressor,
          'Gradient Boosting Regressor': GradientBoostingRegressor}


def convertisseur(bands, bandz):
    """
    Allows the conversion of 3 matrices of square dim 2 or rectangle to a 
    matrix colone 
    
    Parameters
    ----------
    bands : ndarray
        matrice of bands
    bandz : array
        matrice of bathymetry

    Returns
    -------
    ndarray
        the matrice colone and the coordinates points

    """
    l,c = bands[0].shape
    coo = []
    b1 = []
    b2 = []
    b3 = []
    label = []

    for i in range(l):
        for j in range(c):
            three = []
            for x in bands:
                three.append(x[i,j])
            if not(np.isnan(np.amax(np.array(three)))):
                coo.append([i,j])
                b1.append(three[0])
                b2.append(three[1])
                b3.append(three[2])
                label.append(bandz[i,j])

    return [np.array(b1),np.array(b2),np.array(b3)], coo, np.array(label)


def convertisseur2(bands):
    """
    Allows the conversion of 3 matrices of square dim 2 or rectangle to a 
    matrix colone 
    
    Parameters
    ----------
    bands: ndarray
        matrice of x

    Returns
    -------
    ndarray
        the matrice colone and the coordinates points

    """
    l,c = bands[0].shape
    coo = []
    b1 = []
    b2 = []
    b3 = []

    for i in range(l):
        for j in range(c):
            three = []
            for x in bands:
                three.append(x[i,j])
            if not(np.isnan(np.amax(np.array(three)))):
                coo.append([i,j])
                b1.append(three[0])
                b2.append(three[1])
                b3.append(three[2])

    return [np.array(b1),np.array(b2),np.array(b3)], coo


def DatasetML(BandRef, BandZ):
    """
    Create Dataset for Random Forest

    Parameters
    ----------
    BandRef : list of array
        list of different reflectance band and other arrays for the feature of the dataset.
    BandZ : array
        matrice of bathymetry, or just y value.

    Returns
    -------
    features : ndarray
        Dataset of feature for random forest.
    labels : array
        Dataset of layer for random forest.

    """
    bandc, coo, labels = convertisseur(BandRef, BandZ)
    
    if len(bandc) > 1:
        features = np.stack(bandc, axis=-1)
        features = np.squeeze(features)
    else:
        features = bandc[0]
    
    return [features, labels, coo]


def Dataset(BandRef):
    """
    Create Dataset for Random Forest

    Parameters
    ----------
    BandRef : list of array
        list of different reflectance bands and other arrays for the feature of the dataset.
    BandZ : array
        matrice of bathymetry, or just y value.

    Returns
    -------
    features : ndarray
        Dataset of feature for random forest.
    labels : array
        Dataset of layer for random forest.

    """
    bandc, coo = convertisseur2(BandRef)
    
    features = np.stack(bandc, axis=-1)
    features = np.squeeze(features)

    return [features, coo]


def MachineLearningSklearn(features, labels, ML, test_size = 0.25, train_size=0.75, random_state = 42, n_estimators = 1000):
    """
    Train and test a model of a random forest, by Sklearn

    Parameters
    ----------
    features : ndarray
        array dataset of the feature. It's an array colone with each colone being for the value for the same feature
    labels : ndarray
        array dataset of the label. It's the output of each feature
    test_size : int, optional
        It's the percentage of value in the train and the test. The default is 0.25.
    random_state : int, optional
        It's the number of the seed, to keep the same seed. The default is 42.
    n_estimators : int, optional
        It's the number of estimation, when training the model. The default is 1000.
    
    Returns
    -------
    model : model
        The random forest model trainning.
    """
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size = test_size, train_size=train_size, random_state = random_state)
    train_labels = train_labels.ravel(); test_labels = test_labels.ravel()
    
    if DicoML.get(ML) :
        print(ML)
        model = DicoML[ML](n_estimators = n_estimators, random_state = random_state)
    else:
        print("The algorithm's name of machine learning is wrong or it's not implemented")
        sys.exit()
        
    model.fit(train_features, train_labels)
    predictions = model.predict(test_features)
    
    rmse = np.sqrt(mean_squared_error(test_labels,predictions))
    errors = abs(predictions - test_labels)
    print('RMSE : ', rmse)
    print('Mean Absolute Error :', round(np.mean(errors), 2), 'meters.')
    print('Median Absolute Error :', round(np.median(errors), 2), 'meters.')
    mape = 100 * (errors / test_labels)
    accuracy = 100 + np.mean(mape)
    print('Accuracy :', round(accuracy, 2), '%.')
    r2 = model.score(train_features, train_labels)
    print("R2 : ", r2)
            
    return model


def Estimation(model, data, info_predict):
    """
    Save the prediction of the model

    Parameters
    ----------
    model : model
        The model used.
    data : array colonne
        array with data to predict.
    info_predict : list
        list of different information [coordinate, l, c].
        coordinate is the list of coordinates of each value, which will be predicted

    Returns
    -------
    mat_est : array
        matrice of prediction

    """
    estimation = model.predict(data)
    coo = info_predict[0]; l = info_predict[1]; c = info_predict[2]
    mat_est = np.zeros((l,c))
    if len(coo) == estimation.shape[0]:
        nb = len(coo)
        for i in range(nb):
            mat_est[coo[i][0],coo[i][1]] = estimation[i]
        return mat_est
    else:
        print("Problem : ", len(coo), "!=", estimation.shape[0])
        sys.exti()
    

def SavePredict(mat_predict, title):
    """
    Save the prediction of the model

    Parameters
    ----------
    mat_predict : array
        matrice of bathymetry predict by the model
    title : string
        The title of the image.

    Returns
    -------
    None.

    """
    iio.imsave(title, mat_predict)
            

def CutPredict(mat_predict, poscut, bandz, titlecut):
    """
    Make a graph of a cut in the reel et predic bathymetry

    Parameters
    ----------
    mat_predict : array
        matrice of bathymetry predict by the model
    poscut : int, optional
        Position of the cut in the matrice. The default is 0.
    bandz : array
        Band of reel bathymetry.
    titlecut : string, optional
        Title of the graph. The default is 'cut.tif'.

    Returns
    -------
    None.

    """
    y1 = bandz[:,poscut]
    y2 = mat_predict[:,poscut]
    y1 = np.where(y1==0,np.nan, y1)
    y2 = np.where(y2==0,np.nan, y2)
    plot.PlotCut(y1,y2, title=titlecut)