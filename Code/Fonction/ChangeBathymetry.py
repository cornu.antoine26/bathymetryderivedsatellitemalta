# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 14:11:07 2022

@author: Antoine Cornu student of ENSG France
"""

import imageio as iio
import numpy as np
import Fonction.LowFilter as lf


def ChangeBathyArea(bathy):
    """
    Remodel of zones defined in bounds

    Parameters
    ----------
    bathy : array
        image with all bands with bathymetry band at the end.

    Returns
    -------
    bathy : array
        image of bathymetry receipt.

    """
    Bounds = [[242, 296, 217, 253], #for the original image 500x500
              [208, 250, 165, 185],
              [190, 202, 193, 204],
              [172, 192, 205, 216],
              [162, 187, 217, 230],
              [170, 183, 230, 271],
              [187, 201, 317, 330],
              [202, 214, 326, 340],
              [215, 235, 330, 354]]
    
    Type = ['H','H','H','V','V','V','H','H','V'] #to use algorithme horizontal or vertical
    Filtre = [3,3,3,3,3,3,1,1,1] #number of pixel take the kernel filter more on the border
    
    for i in range(9):
        bathy = DelBump(bathy, Bounds[i],Type[i],Filtre[i])
    
    return bathy


def DelBump(img, bound, HV, f):
    """
    Correction of the area in the Bound, relative to horizontal or vertical

    Parameters
    ----------
    img : array
        image of bathymetry.
    bound : list
        list of [min_l, max_l, min_c, max_c]. l fot ligne and c for colonne
    f : int
        the number of pixels, to expand the area of application of the filter.

    Returns
    -------
    img : array
        image of bathymetry receipt.

    """
    if HV == 'H':
        img = horizontal(img, bound, f)
    if HV == 'V':
        img = vertical(img, bound, f)
    
    return img


def horizontal(img, bound, f):
    """
    Correction of the area in the Bound

    Parameters
    ----------
    img : array
        image of bathymetry.
    bound : list
        list of [min_l, max_l, min_c, max_c]. l fot ligne and c for colonne
    f : int
        the number of pixels, to expand the area of application of the filter.

    Returns
    -------
    img : array
        image of bathymetry receipt.

    """
    lengh_c = bound[2]-bound[3]
    for i in range(bound[0],bound[1]+1):
        val_left = img[i,bound[2]-1]
        val_right = img[i,bound[3]+1]
        ecart = (val_left - val_right)/lengh_c
        for j in range(bound[2], bound[3]+1):
            img[i,j] = img[i,j-1]+ecart 
    
    img = lf.KernelFilter(img,bound, f)
    return img


def vertical(img, bound, f):
    """
    Correction of the area in the Bound

    Parameters
    ----------
    img : array
        image of bathymetry.
    bound : list
        list of [min_l, max_l, min_c, max_c].l fot ligne and c for colonne
    f : int
        the number of pixels, to expand the area of application of the filter.

    Returns
    -------
    img : array
        image of bathymetry receipt.

    """
    lengh_l = bound[0]-bound[1]
    for i in range(bound[2],bound[3]+1):
        val_up = img[bound[0]-1,i]
        val_down = img[bound[1]+1,i]
        ecart = (val_up - val_down)/lengh_l
        for j in range(bound[0], bound[1]+1):
            img[j,i] = img[j-1,i]+ecart 
    
    img = lf.KernelFilter(img,bound, f)
    return img

