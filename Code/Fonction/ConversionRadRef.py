# -*- coding: utf-8 -*-
"""
Created on Fri May 27 15:01:10 2022

@author: Antoine Cornu student of ENSG France
"""

from lxml import etree

import numpy as np

def splitTime(T):
    """
    Split the time string in time float

    Parameters
    ----------
    T : string
        the time of the metadata of the image.

    Returns
    -------
    list of float
        [YYYY,MM,DD,hh,mm,ss.dddd].

    """
    t1 = T.split("T") #separetion of YYYY-MM-DD and hh:mm:ss.dddddZ
    t2 = t1[1].split("Z")[0] #delete the Z
    t3 = t1[0].split("-") # separation of YYYY MM DD
    t4 = t2.split(":") # separation of hh mm ss.dddd
    return [float(t3[0]),float(t3[1]),float(t3[2]),float(t4[0]),float(t4[1]),float(t4[2])]


def distSunEarth(YYYY,MM,DD,hh,mm,ss):
    """
    Calculate the distance between the earth and the sun on one specified day

    Parameters
    ----------
    YYYY : int
        year.
    MM : int
        month.
    DD : int
        day.
    hh : int
        hour.
    mm : int
        minute.
    ss : float
        second.

    Returns
    -------
    dES : float
        distance earth sun.

    """
    UT = hh+mm/60+ss/3600
    YYYY = YYYY-1
    MM = MM + 12
    A = int(YYYY/100)
    B = 2-A+int(A/4)
    JD = int(365.25*(YYYY + 4716)) + int(30.6001 * (MM + 1)) + DD + UT/24 + B - 1524.5
    D = JD - 2451545
    g = 357.529 + 0.98560028*D
    dES = 1.00014 - 0.01671*np.cos(g*np.pi/180) - 0.00014*np.cos(2*g*np.pi/180)
    return dES


def convRadToRefTOA(Bands, xmlpath, data, listbands):
    """
    Blue and Green Band TOA Reflection Radiation Conversion

    Parameters
    ----------
    Bands : list array
        bands list of image original [B,G].
    xmlpath : string
        path of the xml metadata of the image.
    data : dico
        dictionary of values need for the calculate.
    listbands : list of string
        list of different band which will be converted into reflectance ex: listbands = ['B','G']

    Returns
    -------
    imgReflectance : list of the array
        it contains 2 array one is the reflection bleu and the second the reflection green.

    """
    imgReflectance = []
    tree = etree.parse(xmlpath)
    for t in tree.xpath('/isd/IMD/IMAGE/FIRSTLINETIME'): #Research time in the metadata
        TimeImg = t.text
    T = splitTime(TimeImg)
    dse = distSunEarth(T[0],T[1],T[2],T[3],T[4],T[5])
    nb_b = len(Bands)
    for i in range(nb_b):
        b1 = Bands[i]
        for abscalfactore in tree.xpath(f'/isd/IMD/BAND_{listbands[i]}/ABSCALFACTOR'):
            a = float(abscalfactore.text)
        for ebw in tree.xpath(f'/isd/IMD/BAND_{listbands[i]}/EFFECTIVEBANDWIDTH'):
            b = float(ebw.text)
        for mse in tree.xpath('/isd/IMD/IMAGE/MEANSUNEL'):
            zenithsun = 90 - float(mse.text)
        L = data[f"GAIN_{listbands[i]}"]*b1*(a/b) - data[f"OFFSET_{listbands[i]}"]
        rho = (np.pi*L*(dse**2))/(data[f"ESNU_{listbands[i]}"]*np.cos(zenithsun*np.pi/180))
        imgReflectance.append(rho)
        
    return imgReflectance


def convRadToRef(Bands, xmlpath, data, listbands):
    """
    Blue and Green Band remote sensing Reflection Radiation Conversion

    Parameters
    ----------
    Bands : list of array
        bands list of image original [B,G].
    xmlpath : string
        path of the xml metadata of the image.
    data : dico
        dictionary of values needed for the calculation
    listbands : list of string
        list of different band which will be converted into reflectance ex: listbands = ['B','G']

    Returns
    -------
    imgReflectance : list of array
        it contains 2 array one is the reflection bleu and the second the reflection green.

    """
    imgReflectance = []
    tree = etree.parse(xmlpath)
    # for t in tree.xpath('/isd/IMD/IMAGE/FIRSTLINETIME'): #Research time in the metadata
    #     TimeImg = t.text
    # T = splitTime(TimeImg)
    # dse = distSunEarth(T[0],T[1],T[2],T[3],T[4],T[5])
    nb_b = len(Bands)
    for i in range(nb_b):
        b1 = Bands[i]
        if listbands[i]=='B':
            Wavelength = 0.4819
        else:
            Wavelength = 0.5471
        for abscalfactore in tree.xpath(f'/isd/IMD/BAND_{listbands[i]}/ABSCALFACTOR'):
            a = float(abscalfactore.text)
        for ebw in tree.xpath(f'/isd/IMD/BAND_{listbands[i]}/EFFECTIVEBANDWIDTH'):
            b = float(ebw.text)
        for meansatel in tree.xpath('/isd/IMD/IMAGE/MEANSATEL'):
            satze = 90 - float(meansatel.text)
        Rsky = RSKY(satze)
        # xa, xb, xc, Edir, Edif = Pyatm.AtmosphereWV3(Wavelength)
        # L = data[f"GAIN_{listbands[i]}"]*b1*(a/b) - data[f"OFFSET_{listbands[i]}"]
        # y = xa*L - xb
        # rhotoc = y/(1+xc*y)
        # Rrs = rhotoc/np.pi - (Rsky*Edif)/(np.pi*(Edir+Edif))
        # print((Rsky*Edif)/(np.pi*(Edir+Edif)))
        # imgReflectance.append(L)
        
    return imgReflectance


def RSKY(viewsatzenith):
    radanglevsz = viewsatzenith*np.pi/180
    radanglevsf = np.arcsin((1/1.34)*np.sin(radanglevsz))
    numsin = np.sin(radanglevsz - radanglevsf)
    demsin = np.sin(radanglevsz + radanglevsf)
    numtan = np.tan(radanglevsz - radanglevsf)
    demtan = np.tan(radanglevsz + radanglevsf)
    Rsky = 1/2*((numsin/demsin)**2 + (numtan/demtan)**3)
    return round(Rsky,4)