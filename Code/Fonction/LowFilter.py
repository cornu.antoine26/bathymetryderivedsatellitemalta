# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 15:01:15 2022

@author: Antoine Cornu student of ENSG France
"""

from scipy import ndimage
import numpy as np

kg5 = (1/256)*np.array([[1,4,6,4,1],
                        [4,16,24,16,4],
                        [6,24,36,24,6],
                        [4,16,24,16,4],
                        [1,4,6,4,1]])

def KernelFilter(img,bound, f):
    """
    Application of a kernel filter gaussian on the area with f pixel more on the bound

    Parameters
    ----------
    img : array
        image of bathymetry.
    bound : list of int
        list of [min_l, max_l, min_c, max_c].l fot ligne and c for colonne
    f : int
        The number of pixels, to expand the area of application of the filter.

    Returns
    -------
    img : array
        image of bathymetry receipt.

    """
    img[bound[0]-f:bound[1]+f,bound[2]-f:bound[3]+f] = ndimage.convolve(img[bound[0]-f:bound[1]+f,bound[2]-f:bound[3]+f], kg5, mode='reflect', cval=0.0)
    return img


def PassBas(img, band, one=False):
    """
    Application of a kernel filter on an image, it's a low pass

    Parameters
    ----------
    img : array
        image with different bands, the band is the first dimension of the array.
    band : int
        the number of the bands, do you want to apply the lower filter.
    one : boolean, optional
        if there is just one band to apply the filter. The default is False.

    Returns
    -------
    img : array
        image with filtered band.

    """
    if one:
        img[band,:,:]=ndimage.convolve(img[band,:,:], kg5, mode='reflect', cval=0.0)
    else:
        for i in range(band):
            img[i,:,:]=ndimage.convolve(img[i,:,:], kg5, mode='reflect', cval=0.0)
    
    return img