# -*- coding: utf-8 -*-
"""
Created on Fri May 27 14:42:04 2022

@author: Antoine Cornu student of ENSG France
"""
###############################################################################
################## MAIN TO TEST AND DEVELOP A SKLEARN MODEL ##################
################## TO CALCULATE BATHYMETRY BY AERIAL IMAGE ####################
###############################################################################
"""
Python file allows to create and evaluate a Random forest regressor model from 
satellite image here example with Worldview-3 or sentinel2 level-C1 images 
"""

############# Import and different python librairy need #############
"""
Python librairy you need:
numpy, imageio, lxml, scikit-learn, matplotlib, gdal, joblib
"""
import sys
import Fonction.LoadDataGdal as ldg
import Fonction.Masquing as mask
import Fonction.ProcessingData as pd
import Fonction.MachineLearningRegressor as mlr
import Fonction.LowFilter as lf
import joblib as jlib
import imageio as iio
import numpy as np

###############################################################################
################################ Parameters ###################################

######## Parameters to calculate reflectance in image from radiometry #########
"""
If you have bands of sentinel2 level-C1, you don't need this parameter or
if you use the radiation of the image

I need a lot of information to calculate the reflection of satellite images
Here work on Worldview-3 images and all information is on : 
https://dg-cms-uploads-production.s3.amazonaws.com/uploads/document/file/207/Radiometric_Use_of_WorldView-3_v2.pdf

The Gain and Offset are the absolute radiometric calibration band dependent 
adjustment factors
GAIN_B = for the bleu band
GAIN_G = for the green band

ESNU is the band-averaged solar exoatmopsheric irradiance in W/m2/µm 
(_B blue, _G green band)
also called, Spectral Irradiance
"""

Gain_B = 0.905
Offset_B = -4.189
Gain_G = 0.907
Offset_G = -3.287
Gain_R = 0.945
Offset_R = -1.350
ESNU_B = 2004.610
ESNU_G = 1830.180
ESNU_R = 1535.33

Data = {'GAIN_B':Gain_B,
        'GAIN_G':Gain_G,
        'GAIN_R':Gain_R,
        'OFFSET_B':Offset_B,
        'OFFSET_G':Offset_G,
        'OFFSET_R':Offset_R,
        'ESNU_B':ESNU_B,
        'ESNU_G':ESNU_G,
        'ESNU_R':ESNU_R}


####################### Parameters to import data #############################
"""
The path of metadata in the image, file XML, in pathxml.

The path of the image, in pathimg and nb_bands is the number of bands there 
are in the image.

The path of the bathymetry data, in pathbathy.

If you are bands of image, put True to the variable JustBand and complet variables
You can leave the pathimg empty (= '') and same for tapes if you have the full image

If your data is sentienl2-L1C, put True in the variable Data_sentinel2_l3A and 
you don't need XML.
"""
pathxml = '../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS-011098385010_01_P001.XML'

JustBand = False

#Worldview 3 image
pathimg = "../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS_R1C1-011098385010_01_P001.tif"

#Sentinel-2 L2A image Theia
path_B = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B2.tif"
path_G = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"
path_R = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B4.tif"
path_Pir = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"

if JustBand:
    pathimg = [path_B, path_G, path_R, path_Pir]

pathbathy = "../Dataset/DEM/Bathymetric data of Ramla Bay.tif"

Data_sentinel2 = False

######################### Parameters data processing ##########################

####### Parameter for bands image #######
"""
What band do you want to use to train the algorithm?
Bands: 
    Blue: B
    Green: G
    Red: R

ex : bandref = ['B', 'G', 'R']

nb_band : number of band with bathymetry B,G,R,Pir,Z 
(exemple with data of Worldview 3)
"""
bandref = ['B','G','R']
nb_band = 5

####### Parameter to applicate a low filter on image data ######
"""
Application of a kernel filter on radiative data before making processing 
to calculate the TOA reflectance
"""
KernelFilter = True

####### Parameter for data mask #######
"""
There are several masks we apply to the data, True for application and False 
no application:   
    MaskDepth: to keep data with bathymetry, moreover you can choose 
                an interval of value to keep
        maxDepth, minDepth: by default, 0 to keep all data under 0, 
                            when minDepth != 0 there is an interval, 
                            moreover maxDepth and minDepth <= 0

    MaskPlant : to separate aquatic plant and sand with the band blue
        threshold_B: threshold of the separation, the value of radiation in 
                        the image, 202 by default.
"""
MaskDepth = True
maxDepth = 0
minDepth = -10

MaskPlant = True
KMeans = True
threshold_B = 202


############ Parameters to add band log(B1)/log(B2) and train on it ###########
"""
Using radiation of image to train the model: RadData = True
For reflectance: RadData = False

It's to try, to train the machine learning model with band log(B1)/log(B2)

Log_Train is a boolean to active the calcul, if is false nothing is calculated
Each other is the factor band you want in the training
"""
RadData = True

Log_Train = False
BlueGreen = True
BlueRed = True
GreenRed = True

####################### Parameters to machine learning model #####################
"""
MachineLearning: string
    It's a name of algorithme of machine learning implemented in the files, 
    by sckitit-image
    list:
        Random Forest Regressor
        Gradient Boosting Regressor

test_size: int
    It's the percentage of value in the train and the test. The default is 0.25.
    
random_state: int
    It's the number of the seed, to keep the same seed. The default is 42.
    
n_estimators: int
    It's the number of estimations when training the model. The default is 1000.
"""
MachineLearning = 'Random Forest Regressor'
test_size = 0.25
train_size = 0.75
random_state = 42
n_estimators = 1000

####### Parameter to delete bump of bathmetry data #######
"""
it is predefined areas for just the type of image in this test to remove the 
biggest bumps
True for DeletionBigBump to make this,
Save it to save the image changed, and the title is the name of the image saved, 
if it's saving
"""
DeletionBigBump = True
title = 'BBathy.tif'

####### Parameter for vignette ########
"""
It's for training the algorithm on a vignette of the image.
To use using_vignette = True, else using_vignette = False.

The vignette is defined by x,y in the up-left corner (x_ul, y_ul), and x,y 
at the down-right corner (x_dr, y_dr).
vignette = [x_ul, x_dr, y_ul, y_dr]
"""
using_vignette = True

#WV3
vignette = [1000,2200,2800,4096] ; str1 = 'all data'
# vignette = [1785,2042,3452,3816] ; str = 'average area'
# vignette = [1838,2110,3528,3711] ; str = 'little area' 

#S2
# vignette = [10819,10903,3512,3592] ; str = 'all data'
# vignette = [10865,10892,3531,3574] ; str = 'average area'
# vignette = [10870,10889,3556,3572] ; str = 'little area'

######## Parameter of save image #########
"""
save_predict is to load the image of the prediction
if True:
    you need the title of image(s), don't forget the extension (.tif)
"""
save_predict = False
titleImg_p = 'Prediction/Img_predict.tif'
titleImgSand_p = 'Prediction/Img_predict_s.tif'
titleImgPlant_p = 'Prediction/Img_predict_p.tif'

######## Parameter of differentes Plot for visualisation #########
"""
PlotCut is to plot two sections, one in the bathymetry in blue and another in 
the prediction in orange
"""
PlotCut = False
poscut = 800
titleCut = 'Diagram_Cut'
titleCutSand = 'Diagram_Sand_Cut'
titleCutPlant = 'Diagram_Plant_Cut'

######## Parameter to save the bathymetry associate ########
"""
True to save the bathymetry
if True:
    you need the title of image, don't forget the extension (.tif)
"""
save_depth = False
titleDepth = "img_depth.tif"

######## Parameter to save the model created #######
"""
True to save the model(s)
if True:
    you need the title of model(s), don't forget the extension ".sav" for a model
"""
save_model = True
modelName = 'Model_ML/Model_allWV3.sav'
modelNameSand = 'Model_ML/Model_sandWV3.sav'
modelNamePlant = 'Model_ML/Model_plantWV3.sav'

####### GDAL processing parameter #####
"""
useprocessingGDAL is a boolean
True to use gdal processing of images by GDAL on python, this process may not work, 
if it does not work, you must do the processing on QGIS before and put the variable in 
False and put the path to the processed image to the variable pathimgpro.

For the processing see the document "Guide QGIS processing data"
"""
useprocessingGDAL = False
pathimgpro = '../Dataset/Processing_Data/AllBandsWV3&Bathy.tif'

######## Parameter #######
"""
This is the coordinate of a sand pixel,
it's to recognize sand pixels and plant pixels in the classification
"""
# coor = [[10890,3563]] #S2 Theia
coor = [[1968,3750]] #WV3 all


################################# Configuration ###############################
if Data_sentinel2:
    pos_bandz = 4
    if not(KMeans):
        MaskPlant = False
else:
    pos_bandz = 4

###############################################################################
############################### Data processing ###############################

############################### Load images ###################################
if useprocessingGDAL:
    img = ldg.Download_Data(pathimg, pathbathy, JustBand, Data_sentinel2, DeletionBigBump, title)
    ldg.verif(img)
else:
    imggdal, img = ldg.Load_imggdalmultiband(pathimgpro)

############################### Application Low Filter ########################
if KernelFilter:
    img1 = lf.PassBas(img, pos_bandz)

############################### Bands extraction ##############################
Bands = pd.Bands(img1)

########################### Processing Data ###################################
nbBand = len(bandref)

if Data_sentinel2:
    BandsRef = Bands
    posB = 0
else:
    if RadData:
        BandsRef = Bands
        posB = 0
    else:
        BandsRef = pd.WV3ref(Bands, pathxml, Data, bandref)
        posB = -1
        pos_bandz = len(BandsRef)-2
        
############################ Add band log(B1)/log(B2) #########################
if Log_Train :
    BandsLog = []
    nbBand = 0
    
    if BlueGreen:
        BandsLog.append(pd.logB1divlogB2(BandsRef[0],BandsRef[1]))
        nbBand += 1
        
    if BlueRed :
        BandsLog.append(pd.logB1divlogB2(BandsRef[0],BandsRef[2]))
        nbBand += 1
        
    if GreenRed :
        BandsLog.append(pd.logB1divlogB2(BandsRef[1],BandsRef[2]))
        nbBand += 1
    
    BandsLog.append(BandsRef[pos_bandz])
    BandsLog.append(BandsRef[0])
    BandsRef = BandsLog
    pos_bandz = nbBand
    posB = -1

############################### Bands masquing ################################
if MaskDepth:
    MaskBands = mask.MaskDepth(BandsRef, BandsRef[pos_bandz], S=0, Depth=0)
    
if MaskPlant:
    if KMeans:
        Sand, Plant, O = mask.MaskPlantSand(MaskBands, posB, pos_bandz, coor)
    else:
        Sand, Plant = mask.MaskPlant(MaskBands, Bands[0], rad = 202)
        
    if MaskDepth:
        if minDepth != 0:
            Sand = mask.MaskDepth(Sand, Sand[pos_bandz], S=maxDepth, Depth=minDepth)
            Plant = mask.MaskDepth(Plant, Plant[pos_bandz], S=maxDepth, Depth=minDepth)
            
    ######## Test on little image ######
    nb = len(Sand)
    if using_vignette:
        for b in range(nb):
            Sand[b] = Sand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
            Plant[b] = Plant[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]

# else:
if MaskDepth:
    if minDepth != 0:
        MaskBands = mask.MaskDepth(MaskBands, MaskBands[pos_bandz], S=maxDepth, Depth=minDepth)
        
######## Test on little image #####
if using_vignette:
    nb = len(MaskBands)
    for b in range(nb):
        MaskBands[b] = MaskBands[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]

###############################################################################
########################### Machine Learning Model ############################
"""
Creation of the model you want
"""

if MaskPlant:
    Datasetsand = mlr.DatasetML(Sand[0:nbBand], Sand[pos_bandz])
    Datasetplant = mlr.DatasetML(Plant[0:nbBand], Plant[pos_bandz])
    
    l,c = Sand[0]
    infops = [Datasetsand[2],l,c]
    infopp = [Datasetplant[2],l,c]

    print('Results Evaluation Model with sand data')
    modelSand = mlr.MachineLearningSklearn(Datasetsand[0],Datasetsand[1], ML=MachineLearning, test_size = test_size, random_state = random_state, n_estimators = n_estimators, save_predict=save_predict, info_predic=infops, title = titleImgSand_p)
    print('Results Evaluation Model with plant data')
    modelPlant = mlr.MachineLearningSklearn(Datasetplant[0],Datasetplant[1], ML=MachineLearning, test_size = test_size, random_state = random_state, n_estimators = n_estimators, save_predict=save_predict, info_predic=infopp, title = titleImgPlant_p)
    
    if save_predict or PlotCut:
        matPredictSand = mlr.Estimation(modelSand, Datasetsand[0], infops)
        matPredictPlant = mlr.Estimation(modelPlant, Datasetplant[0], infopp)
        
        if save_predict:
            mlr.SavePredict(matPredictSand, titleImgSand_p)
            mlr.SavePredict(matPredictSand, titleImgPlant_p)
            
            if save_depth:
                iio.imsave(titleDepth, MaskBands[pos_bandz])

        if PlotCut:
            mlr.CutPredict(matPredictSand, poscut, Sand[pos_bandz], titleCutSand)
            mlr.CutPredict(matPredictSand, poscut, Plant[pos_bandz], titleCutPlant)

    if save_model:
        jlib.dump(modelSand, modelNameSand)
        jlib.dump(modelPlant, modelNamePlant)

else:
    Dataset = mlr.DatasetML(MaskBands[0:nbBand], MaskBands[pos_bandz])
    l,c = Sand[0]
    info_predic = [Dataset[2],l,c] 
    print('Results Evaluation Model with all data')
    modelRF = mlr.MachineLearningSklearn(Dataset[0],Dataset[1], ML=MachineLearning, test_size = test_size, random_state = random_state, n_estimators = n_estimators, save_predict=save_predict, info_predic=info_predic, title = titleImg_p)
    
    if save_predict or PlotCut:
        matPredict = mlr.Estimation(modelRF, Datasetsand[0], infops)
        
        if save_predict:
            mlr.SavePredict(matPredict, titleImg_p)
            
            if save_depth:
                iio.imsave(titleDepth, MaskBands[pos_bandz])

        if PlotCut:
            mlr.CutPredict(matPredict, poscut, Sand[pos_bandz], titleCut)

    if save_model:
        jlib.dump(modelRF, modelName)

############ To make 3 models in same time all sand and plant #################
# """
# To do this cancel the code below, and comment the one above titled Machine Learning Model
# After running line 329 (else:) and tab down the 3 rows
# And MaskPlant = True (line 138)
# Now you can run the code
# """

# Datasetsand = mlr.DatasetML(Sand[0:nbBand], Sand[pos_bandz])
# Datasetplant = mlr.DatasetML(Plant[0:nbBand], Plant[pos_bandz])
# Dataset = mlr.DatasetML(MaskBands[0:nbBand], MaskBands[pos_bandz])
# l,c = MaskBands[0].shape
# info_ps = [Datasetsand[2],l,c]
# info_pp = [Datasetplant[2],l,c]
# info_predict = [Dataset[2],l,c]
# print('all data')
# modelRF = mlr.MachineLearningSklearn(Dataset[0],Dataset[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)
# print('Data Sand')
# modelRFSand = mlr.MachineLearningSklearn(Datasetsand[0],Datasetsand[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)
# print('Data Plant')
# modelRFPlant = mlr.MachineLearningSklearn(Datasetplant[0],Datasetplant[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)

# MachineLearning = 'Gradient Boosting Regressor'

# print('all data')
# modelGB = mlr.MachineLearningSklearn(Dataset[0],Dataset[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)
# print('Data Sand')
# modelGBSand = mlr.MachineLearningSklearn(Datasetsand[0],Datasetsand[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)
# print('Data Plant')
# modelGBPlant = mlr.MachineLearningSklearn(Datasetplant[0],Datasetplant[1], ML=MachineLearning, test_size = test_size, train_size=train_size, random_state = random_state, n_estimators = n_estimators)


# if save_predict or PlotCut:
#     matrf = mlr.Estimation(modelRF, Dataset[0], info_predict)
#     matrfs = mlr.Estimation(modelRFSand, Datasetsand[0], info_ps)
#     matrfp = mlr.Estimation(modelRFPlant, Datasetplant[0], info_pp)
#     matgb = mlr.Estimation(modelGB, Dataset[0], info_predict)
#     matgbs = mlr.Estimation(modelGBSand, Datasetsand[0], info_ps)
#     matgbp = mlr.Estimation(modelGBPlant, Datasetplant[0], info_pp)
    
#     if save_predict:
#         mlr.SavePredict(matrf, 'imgp_rf_allall_S2.tif')
#         mlr.SavePredict(matrfs, 'imgp_rf_sandall_S2.tif')
#         mlr.SavePredict(matrfp, 'imgp_rf_plantal_lS2.tif')
#         mlr.SavePredict(matgb, 'imgp_gb_allall_S2.tif')
#         mlr.SavePredict(matgbs, 'imgp_gb_sandall_S2.tif')
#         mlr.SavePredict(matgbp, 'imgp_gb_plantall_S2.tif')
#         iio.imsave("img_depth_all_S2.tif", MaskBands[pos_bandz])

#     if PlotCut:
#         mlr.CutPredict(matrf, poscut, MaskBands[pos_bandz], 'diacut_rf_all_all_S2')
#         mlr.CutPredict(matrfs, poscut, Sand[pos_bandz], 'diacut_rf_sand_all_S2')
#         mlr.CutPredict(matrfp, poscut, Plant[pos_bandz], 'diacut_rf_plant_all_S2')
#         mlr.CutPredict(matgb, poscut, MaskBands[pos_bandz], 'diacut_gb_all_all_S2')
#         mlr.CutPredict(matgbs, poscut, Sand[pos_bandz], 'diacut_gb_sand_all_S2')
#         mlr.CutPredict(matgbp, poscut, Plant[pos_bandz], 'diacut_gb_plant_all_S2')

# if save_model:
#     jlib.dump(modelRF, 'ModelRF_all01080WV3.sav')
#     jlib.dump(modelRFSand, 'ModelRF_allsand01080WV3.sav')
#     jlib.dump(modelRFPlant, 'ModelRF_allplant01080WV3.sav')
#     jlib.dump(modelGB, 'ModelGB_all01080WV3.sav')
#     jlib.dump(modelGBSand, 'ModelGB_allsand01080WV3.sav')
#     jlib.dump(modelGBPlant, 'ModelGB_allplant01080WV3.sav')

############# Print indication #############

if minDepth != 0:
    print('area study between {} and {} meter of depth.'.format(maxDepth,minDepth))
else:
    print('area study {}'.format(str1))



