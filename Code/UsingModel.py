# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 13:10:48 2022

@author: Antoine Cornu student of ENSG France
"""

###############################################################################
############################# MAIN TO USE A MODEL #############################
################## TO CALCULATE BATHYMETRY BY AERIAL IMAGE ####################
###############################################################################
"""
Python file allows to use a model on data like Worldview 3.

this document uses the 3 models, the one without classification and 
the 2 with classification, to release three images of prediction, 
one without classification, one with sand classification and 
one with plant classification.

If you want to use a single model you just have to comment on the lines of 
the models you didn’t want in the part Application model line 129
all for the model training without classification
sand for the model training with classification sand
plant for the model training with classification plant
"""

############# Import and different python librairy need #############
"""
Python librairy you need:
numpy, joblib, scikit, gdal, imageio, matplotlib
"""
import sys
import imageio as iio
import joblib as jlib
import Fonction.ProcessingData as pd
import Fonction.LowFilter as lf
import Fonction.LoadDataGdal as ldg
import Fonction.Masquing as mask
import Fonction.MachineLearningRegressor as mlr

############ Model & Image ##########
"""
Path to the model .sav
"""
#model RF ou GB
pathmodel = "Model_ML/ModelRF_allWV3.sav"
pathmodelsand = "Model_ML/ModelRF_allsandWV3.sav"
pathmodelplant = "Model_ML/ModelRF_allplantWV3.sav"

#Worldview 3 image
pathimg = "../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS_R1C1-011098385010_01_P001.tif"

####### Parameter for bands image #######
"""
What band do you want to use to train the algorithm?
Bands: 
    Blue: B
    Green: G
    Red: R

ex: bandref = ['B', 'G', 'R']

nb_band: number of band with bathymetry B, G, R, Pir, Z (exemple with data of Worldview 3)
"""
bandref = ['B','G','R']
nb_band = 5

####### Parameter to applicate a low filter on image data ######
"""
Application of a kernel filter on radiative data before making processing 
to calculate the TOA reflectance.
"""
KernelFilter = True

####### Parameter for data mask #######
"""
There are several masks we apply to the data, True for application and False no application:   
    MaskWater: to separate water and land
    Usingkm: to use a model already existing
"""
MaskWater = True

Usingkm = True
#model kmean path
pathkm = "Model_KM/ModelKMeans.sav"

######## Parameter #######
"""
This is the coordinate of a sand pixel,
it's to recognize sand pixels and plant pixels in the classification
"""
coor = [[1968,3750]] #WV3 R1C1 all
# coor = [[2473,838]] #WV3 R1C2 all
# coor = [[1211,2435]] #WV3 R2C2 all

####### Save Prediction ##########
"""
True to save the prediction with projection in the GIS or just an image
and False without projection
The projection is the same as input data

3 variables for the names of the 3 prediction images to be recorded
"""
geoimage = True
NameImgAll = 'Prediction_WorldView3/ImgR1C1PredictRFAll.tif'
NameImgSand = 'Prediction_WorldView3/ImgPredictRFSandAll.tif'
NameImgPlant = 'Prediction_WorldView3/ImgPredictRFPlantAll.tif'

############################ APPLICATION ######################################

###### Load model #######
model = jlib.load(pathmodel)
modelsand = jlib.load(pathmodelsand)
modelplant = jlib.load(pathmodelplant)

###### Load data #######
imggdal, imgarray = ldg.Load_imggdalmultiband(pathimg)

############################### Application Low Filter ########################
if KernelFilter:
    imgarray = lf.PassBas(imgarray, 4)

############################### Bands extraction ##############################
Bands = pd.Bands(imgarray)

############################### Bands masquing ################################
MaskBand = mask.MaskWater(Bands, Bands[3])

if Usingkm:
    modelkmean = jlib.load(pathkm)
    Sand, Plant = mask.UsingKMeans(modelkmean, MaskBand, coor)
else:
    Sand, Plant, O = mask.MaskPlantSand2(MaskBand, 0, coor)

############################# Application model ##############################

Datasetall = mlr.Dataset(MaskBand[0:3]) #all
Datasetsand = mlr.Dataset(Sand[0:3]) #sand
Datasetplant = mlr.Dataset(Plant[0:3]) #plant

l,c = MaskBand[0].shape

info_predict = [Datasetall[1],l,c] #all
info_ps = [Datasetsand[1],l,c] #sand
info_pp = [Datasetplant[1],l,c] #plant

matmodel = mlr.Estimation(model, Datasetall[0], info_predict) #all
matrfsand = mlr.Estimation(modelsand, Datasetsand[0], info_ps) #sand
matrfplant = mlr.Estimation(modelplant, Datasetplant[0], info_pp) #plant

if geoimage:
    ldg.array2raster(imggdal, NameImgAll, matmodel) #all
    ldg.array2raster(imggdal, NameImgSand, matrfsand) #sand
    ldg.array2raster(imggdal, NameImgPlant, matrfplant) #plant
else:    
    mlr.SavePredict(matmodel, NameImgAll) #all
    mlr.SavePredict(matrfsand, NameImgSand) #sand
    mlr.SavePredict(matrfplant, NameImgPlant) #plant