# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 13:10:48 2022

@author: Antoine Cornu student of ENSG France
"""

###############################################################################
##################### MAIN TO EVALUATE A Log-ratio MODEL ######################
################## TO CALCULATE BATHYMETRY BY AERIAL IMAGE ####################
###############################################################################
"""
Python file allows to use a model on data like worldview 3 
"""

############# Import and different python librairy need #############
"""
Python librairy you need:
numpy, joblib, lxml, scikit, gdal, imageio, matplotlib
"""

import joblib as jlib
import Fonction.ProcessingData as pd
import Fonction.LowFilter as lf
import Fonction.LoadDataGdal as ldg
import Fonction.Masquing as mask
import Fonction.MachineLearningRegressor as mlr
import Fonction.LinearModel as lm

###############################################################################
################################ Parameters ###################################

######## Parameters to calculate reflectance in image from radiometry #########
"""
If you have bands of sentinel2 level-2A, you don't need this parameter

I need a lot of information to calculate the reflection of satellite images
Her work on Woridview-3 images and all information is on : 
https://dg-cms-uploads-production.s3.amazonaws.com/uploads/document/file/207/Radiometric_Use_of_WorldView-3_v2.pdf

The Gain and Offset are the absolute radiometric calibration band dependent 
adjustment factors
GAIN_B = for the bleu band
GAIN_G = for the green band

ESNU is the band-averaged solar exoatmospheric irradiance in W/m2/µm 
(_B blue, _G green band)
also called, Spectral Irradiance
"""

Gain_B = 0.905
Offset_B = -4.189
Gain_G = 0.907
Offset_G = -3.287
ESNU_B = 2004.610
ESNU_G = 1830.180

Data = {'GAIN_B':Gain_B,
        'GAIN_G':Gain_G,
        'OFFSET_B':Offset_B,
        'OFFSET_G':Offset_G,
        'ESNU_B':ESNU_B,
        'ESNU_G':ESNU_G}

############ Model ##########
"""
Path to the model .sav and image
"""
#model path       
pathmodel = "Model_log-ratio/Model_logratio_litS2.sav"
pathmodelsand = "Model_log-ratio/Model_logratio_sandlitS2.sav"
pathmodelplant = "Model_log-ratio/Model_logratio_plantaveS2.sav"
  
####################### Parameters to import data #############################
"""
The path of metadata in the image, file XML, in pathxml.

The path of the image, in pathimg and nb_bands is the number of bands there are
in the image.

The path of the bathymetry data, in pathbathy.

If you are bands of image, put True to the variable JustBand and complet variables.
You can leave the pathimg empty (= '') and the same for tapes if you have the full image.

If your data is sentienl2-L2A, put True in the variable Data_sentinel2 and 
you don't need XML.
"""
pathxml = '../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS-011098385010_01_P001.XML'

JustBand = False

#WorldView-3 image
pathimg = "../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS_R1C1-011098385010_01_P001.tif"

#Sentinel-2 L2A image Theia
path_B = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B2.tif"
path_G = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"
path_Pir = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"


if JustBand:
    pathimg = [path_B, path_G, path_Pir]

pathbathy = "../Dataset/DEM/Bathymetric data of Ramla Bay.tif"

Data_sentinel2 = False

####### Parameter for bands image #######
"""
What band do you want to use to train the algorithm?
Bands: 
    Blue: B
    Green: G
    Red: R

ex: bandref = ['B', 'G', 'R']

nb_band: number of band with bathymetry B,G,R,Pir,Z 
(exemple with data of Worldview 3)
"""
bandref = ['B','G']
nb_band = 5

####### Parameter to applicate a low filter on image data ######
"""
Application of a kernel filter on radiative data before making processing 
to calculate the TOA reflectance
"""
KernelFilter = True

####### Parameter for data mask #######
"""
There are several masks we apply to the data, True for application and False no application:   
    MaskWater: to separate water and land
    MaskPlant: to separate aquatic plant and sand with the band blue with a K-Means
    Usingkm: to use a model already existing
"""
MaskWater = True
MaskPlant = True

Usingkm = True
#model kmean
pathkm = "Model_KM/ModelKMeans.sav"

minDepth = -5
maxDepth = -10

######## Parameter #######
"""
This is the coordinate of a sand pixel,
it's to recognize sand pixels and plant pixels in the classification
"""
coor = [[1968,3750]] #WV3 all
# coor = [[10890,3563]] #S2 Theia

######## Vignette ###########
#WV3
vignette = [1000,2200,2800,4096] ; str = 'all data'
# vignette = [1785,2042,3452,3816] ; str = 'average area'
# vignette = [1838,2110,3528,3711] ; str = 'little area' 

#S2
# vignette = [10819,10903,3512,3592] ; str = 'all data'
# vignette = [10865,10892,3531,3574] ; str = 'average area'
# vignette = [10870,10889,3556,3572] ; str = 'little area'

####### Save Prediction ##########
"""
SavePrediction: True to save the prediction
geoimage: True to save the prediction with projection in the GIS or just an image
and False without projection
The projection is the same as input data

3 variables for the names of the 3 prediction images to be recorded
"""
SavePrediction = True
geoimage = False
NameImgAll = 'Prediction/imgp_rf_allave_testall.tif'
NameImgSand = 'Prediction/imgp_rf_sandave_testall.tif'
NameImgPlant = 'Prediction/imgp_rf_plantave_testall.tif'

######## Parameter of differentes Plot for visualisation #########
"""
PlotCut is to plot two sections, one in the bathymetry in blue and another in 
the prediction in orange
"""
PlotCut = False
poscut = 1000
titleCut = 'Diagram_Cut'
titleCutSand = 'Diagram_Sand_Cut'
titleCutPlant = 'Diagram_Plant_Cut'

####### GDAL processing parameter #####
"""
useprocessingGDAL is a boolean
True to use gdal processing of images by GDAL on python, this process may not work, 
if it does not work, you must do the processing on QGIS before and put the variable in 
False and put the path to the processed image to the variable pathimgpro.

For the processing see the document "Guide QGIS processing data"
"""
useprocessingGDAL = True
pathimgpro = '../Dataset/Processing_Data/AllBandsWV3&Bathy.tif'

###############################################################################
############################ APPLICATION ######################################

###### Load model #######
model = jlib.load(pathmodel)
modelsand = jlib.load(pathmodelsand)
modelplant = jlib.load(pathmodelplant)

############################### Load images ###################################
if useprocessingGDAL:
    img = ldg.Download_Data(pathimg, pathbathy, JustBand, Data_sentinel2, DeletionBigBump, title)
    ldg.verif(img)
else:
    imggdal, img = ldg.Load_imggdalmultiband(pathimgpro)
############################### Application Low Filter ########################
if KernelFilter:
    imgarray = lf.PassBas(imgarray, 4)

############################### Bands extraction ##############################
Bands = pd.Bands(imgarray)

########################### Processing Data ###################################
Bands = pd.ProDataRefLinear(Bands, pathxml, Data, Data_sentinel2)

############################### Bands masquing ################################

MaskBand = mask.MaskDepth(Bands, Bands[4], S=0, Depth=0)

# MaskBand = mask.MaskWater(Bands, Bands[3])

if Usingkm:
    modelkmean = jlib.load(pathkm)
    Sand, Plant = mask.UsingKMeans(modelkmean, MaskBand, coor)
else:
    Sand, Plant, O = mask.MaskPlantSand2(MaskBand, 0, coor)

if maxDepth != 0:
    MaskBand = mask.MaskDepth(MaskBand, MaskBand[4], S=minDepth, Depth=maxDepth)
    Sand = mask.MaskDepth(Sand, Sand[4], S=minDepth, Depth=maxDepth)
    Plant = mask.MaskDepth(Plant, Plant[4], S=minDepth, Depth=maxDepth)

for b in range(6):
    Sand[b] = Sand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
    Plant[b] = Plant[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
    MaskBand[b] = MaskBand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]

############################# Application model ##############################
print("All Data")
matmodel = lm.EvalModel(model, Sand[5], Sand[4])
print("")
print("Sand Data")
matmodelsand = lm.EvalModel(modelsand, Plant[5], Plant[4])
print("")
print("Plant Data")
matmodelplant = lm.EvalModel(modelplant,MaskBand[5], MaskBand[4])

if SavePrediction:
    if geoimage:
        ldg.array2raster(imggdal, NameImgAll, matmodel)
        ldg.array2raster(imggdal, NameImgSand, matrfsand)
        ldg.array2raster(imggdal, NameImgPlant, matrfplant)
    else:    
        mlr.SavePredict(matmodel, NameImgAll)
        mlr.SavePredict(matrfsand, NameImgSand)
        mlr.SavePredict(matrfplant, NameImgPlant)

if PlotCut:
    mlr.CutPredict(matmodel, poscut, MaskBand[4], titleCut)
    mlr.CutPredict(matmodelsand, poscut, Sand[4], titleCutSand)
    mlr.CutPredict(matmodelplant, poscut, Plant[4], titleCutPlant)




