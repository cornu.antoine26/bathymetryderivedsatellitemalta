# -*- coding: utf-8 -*-
"""
Created on Fri May 27 14:42:04 2022

@author: Antoine Cornu student of ENSG France
"""
###############################################################################
################## MAIN TO TEST AND DEVELOP A LINEAR MODEL ###################
################## TO CALCULATE BATHYMETRY BY AERIAL IMAGE ####################
###############################################################################
"""
Python file allows  to create and evaluate a linear regression model from 
satellite image here example with Worldview-3 or sentinel2 level-C1 images 
"""

################## Import and different python librairy need ##################
"""
Python librairy you need:
numpy, imageio, lxml, scikit-learn, matplotlib, gdal, joblib
"""

import Fonction.LoadDataGdal as ldg
import Fonction.Masquing as mask
import Fonction.ProcessingData as pd
import Fonction.LinearModel as lm
import Fonction.LowFilter as lf
import joblib as jlib

###############################################################################
################################ Parameters ###################################

######## Parameters to calculate reflectance in image from radiometry #########
"""
If you have bands of sentinel2 level-2A, you don't need this parameter

I need a lot of information to calculate the reflection of satellite images
Her work on Worldview-3 images and all information is on : 
https://dg-cms-uploads-production.s3.amazonaws.com/uploads/document/file/207/Radiometric_Use_of_WorldView-3_v2.pdf

The Gain and Offset are the absolute radiometric calibration band dependent 
adjustment factors
GAIN_B = for the bleu band
GAIN_G = for the green band

ESNU is the band-averaged solar exoatmospheric irradiance in W/m2/µm 
(_B blue, _G green band)
also called, Spectral Irradiance
"""

Gain_B = 0.905
Offset_B = -4.189
Gain_G = 0.907
Offset_G = -3.287
ESNU_B = 2004.610
ESNU_G = 1830.180

Data = {'GAIN_B':Gain_B,
        'GAIN_G':Gain_G,
        'OFFSET_B':Offset_B,
        'OFFSET_G':Offset_G,
        'ESNU_B':ESNU_B,
        'ESNU_G':ESNU_G}


####################### Parameters to import data #############################
"""
The path of metadata in the image, file XML, in pathxml

The path of the image, in pathimg and nb_bands, is the number of bands there are 
in the image

The path of the bathymetry data, in pathbathy

If you are bands of image, put True to the variable JustBand and complete variables
You can leave the pathimg empty (= '') and the same for tapes if you have the full image

If your data is sentienl2-L2A, put True in the variable Data_sentinel2, and you 
don't need XML
"""
pathxml = '../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS-011098385010_01_P001.XML'

JustBand = True

#WorldView-3 image
pathimg = "../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS_R1C1-011098385010_01_P001.tif"

#Sentinel-2 L2A image Theia
path_B = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B2.tif"
path_G = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"
path_Pir = "../Dataset/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9/SENTINEL2B_20181025-095514-224_L2A_T33SVA_D_V1-9_FRE_B3.tif"


if JustBand:
    pathimg = [path_B, path_G, path_Pir]

pathbathy = "../Dataset/bathymetry_data/Bathymetric data of Ramla Bay.tif"

Data_sentinel2 = True

######################### Parameters data processing ##########################

####### Parameter to applicate a low filter on image data ######
"""
Application of a kernel filter on radiative data before making processing 
to calculate the TOA reflectance
"""
KernelFilter = True

####### Parameter for data mask #######
"""
There are several masks we apply to the data, True for application and False 
no application:
    MaskWater: to separate land and water with the band Pir
        threshold_Pir: threshold of the separation, the value of radiation in 
                        the image, 100 by default.
    
    MaskDepth: to keep data with bathymetry, moreover you can choose 
                an interval of value to keep
        maxalti, minalti: by default 0 to keep all data under 0, when 
                            minalti != 0 there is an interval, 
                            moreover maxDepth and minDepth <= 0

    MaskPlant: to separate aquatic plant and sand with the band blue
        threshold_B: threshold of the separation, the value of radiation in 
                        the image, 202 by default.
"""
MaskWater = True
threshold_Pir = 100

MaskDepth = True
maxalti = 0
minalti = 0

MaskPlant = True
KMeans = True
threshold_B = 202

####### Parameter to delete bump of bathmetry data #######
"""
it is predefined areas for just the type of image in this test to remove the 
biggest bumps
True for DeletionBigBump to make this,
Save it to save the image changed, and the title is the name of the image saved, 
if it's saving
"""
DeletionBigBump = True
title = 'B1.tif'

######## Parameter ratio-log band #########
"""
Coeffience n in the ratio-log band
"""
n = 0.0001

####### Parameter for vignette ########
"""
It's for training the algorithm on a vignette of the image
To use using_vignette = True, else using_vignette = False

The vignette is defined by x,y in the up-left corner (x_ul, y_ul), and x,y at
the down-right corner (x_dr, y_dr).
vignette = [x_ul, x_dr, y_ul, y_dr]
"""
using_vignette = True

#WV3
vignette = [1000,2200,2800,4096] ; str = 'all data'
# vignette = [1785,2042,3452,3816] ; str = 'average area'
# vignette = [1838,2110,3528,3711] ; str = 'little area' 

#S2
# vignette = [10819,10903,3512,3592] ; str = 'all data'
# vignette = [10865,10892,3531,3574] ; str = 'average area'
# vignette = [10870,10889,3556,3572] ; str = 'little area'


######## Parameter of differentes Plot for visualisation #########
"""
plotDia is to plot the point log(rB)/log(rG) in x and bathymetry in y, to see 
the cloud of points for the linear regression

PlotCut is to plot two sections, one in the bathymetry in blue and another in 
the prediction in orange
"""
plotDia = False
titleDia = 'Diagram_point'
titleDiaSand = 'Diagram_Sand'
titleDiaPlant = 'Diagram_Plant'

PlotCut = False
poscut = 746
titleCut = 'Diagram_Cut'
titleCutSand = 'Diagram_Sand_Cut'
titleCutPlant = 'Diagram_Plant_Cut'

######## Parameter of save image #########
"""
save_predict is to load the image of the prediction

save_reel is to load the real image of the bathymetry of the same area of 
the prediction
"""
save_predict = False
titleImg_p = 'Prediction/Img_predict.tif'
titleImgSand_p = 'Prediction/Img_predict_s.tif'
titleImgPlant_p = 'Prediction/Img_predict_p.tif'

save_reel = False
titleImg_r ='Img_reel.tif'
titleImgSand_r = 'Img_reel_s.tif'
titleImgPlant_r = 'Img_reel_p.tif'

######## Parameter to save the model created #######
"""
True to save the model(s)
if True:
    you need the title of the model(s), don't forget the extension ".sav" for a model
"""
save_model = True
modelName = 'Model_log-ratio/Model_logratio_litS2.sav'
modelNameSand = 'Model_log-ratio/Model_logratio_sandlitS2.sav'
modelNamePlant = 'Model_log-ratio/Model_logratio_plantlitS2.sav'

####### GDAL processing parameter #####
"""
useprocessingGDAL is a boolean
True to use gdal processing of images by GDAL on python, this process may not 
work, if it does not work, you must do the processing on QGIS before and put 
the variable in False and put the path to the processed image to the variable 
pathimgpro.

For the processing see the document "Guide QGIS processing data"
"""
useprocessingGDAL = True
pathimgpro = '../Dataset/Processing_Data/AllBandsWV3&Bathy.tif'

######## Parameter #######
"""
This is the coordinate of a sand pixel,
it's to recognize  sand pixels and plant pixels in the classification
"""
# coor = [[10890,3563]] #S2 Theia
coor = [[1968,3750]] #WV3 all

################################# configuration ###############################
if Data_sentinel2:
    pos_bandz = 3
    pos_bandpir = 2
    
    if not(KMeans):
        MaskPlant = False
else:
    pos_bandz = 4
    pos_bandpir = 3

###############################################################################
############################### Data processing ###############################

############################### Load images ###################################
if useprocessingGDAL:
    img = ldg.Download_Data(pathimg, pathbathy, JustBand, Data_sentinel2, DeletionBigBump, title)
    ldg.verif(img)
else:
    imggdal, img = ldg.Load_imggdalmultiband(pathimgpro)

############################### Application Low Filter ########################
if KernelFilter:
    img = lf.PassBas(img, pos_bandz)

############################### Bands extraction ##############################
Bands = pd.Bands(img)

########################### Processing Data ###################################
BandsRef = pd.ProDataRefLinear(Bands, pathxml, Data, Data_sentinel2, n=n)

############################### Bands masquing ################################
if MaskDepth:
    MaskBand = mask.MaskDepth(BandsRef, Bands[pos_bandz], S=0, Depth=0)
    MaskWater = False #attention change legerement value
else:
    MaskBand = BandsRef
    
if MaskWater:
    MaskBand = mask.MaskWater(MaskBand, MaskBand[pos_bandpir])

if MaskPlant:
    if KMeans:
        MaskBands = mask.MaskPlantSand(MaskBand, 0, pos_bandz, coor)
    else:
        MaskBands = mask.MaskPlant(MaskBand, Bands[0])

    Sand = MaskBands[0];Plant = MaskBands[1]
    nb = len(Sand)
    
    if minalti != 0:
        Sand = mask.MaskDepth(Sand, Sand[pos_bandz], S=maxalti, Depth=minalti)
        Plant = mask.MaskDepth(Plant, Plant[pos_bandz], S=maxalti, Depth=minalti)
        
    ######## Test on little image ######
    nb = len(Sand)
    if using_vignette:
        for b in range(nb):
            Sand[b] = Sand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
            Plant[b] = Plant[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]

# else:
if minalti != 0:
    MaskBand = mask.MaskDepth(MaskBand, MaskBand[pos_bandz], S=maxalti, Depth=minalti)

######## Test on little image #####
if using_vignette:
    nb = len(MaskBand)
    for b in range(nb):
        MaskBand[b] = MaskBand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]


###############################################################################
########################### Linear Regression Model ###########################

########## Creation of dataset for evaluation ###########
if MaskPlant:
    if KMeans:
        bandsand = Sand; bandplant = Plant; bando = MaskBand[2]
        bandsand = [bandsand[pos_bandz+1],bandsand[pos_bandz]]
        bandplant = [bandplant[pos_bandz+1],bandplant[pos_bandz]]
        if len(bando)!=0:
            print("Attention bando with values")
    else:
          bandsand = MaskBand[0]; bandplant = MaskBand[1]
          bandsand = [bandsand[pos_bandz+1],bandsand[pos_bandz]]
          bandplant = [bandplant[pos_bandz+1],bandplant[pos_bandz]]
         
    vigband_es = []
    vigband_ep = []
    for i in bandsand:
            # vigband_es.append(i[700:1000,910:1060])
            vigband_es.append(i[0:100,0:100])
    for j in bandplant:
            # vigband_ep.append(j[700:1000,910:1060])
            vigband_ep.append(j[0:100,0:100])
else:
    MaskBand = [MaskBand[pos_bandz+1],MaskBand[pos_bandz]]
    bands2_e = MaskBand

############ Apply model ##############
if MaskPlant:
    print("Data Sand :")
    modelS = lm.UseLinearModel(bandsand,vigband_es, plotDia=plotDia, titleDia=titleDiaSand, save_predic=save_predict, titleImg_p=titleImgSand_p, save_reel=save_reel, titleImg_r=titleImgSand_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCutSand)
    print("Data Plant :")
    modelP = lm.UseLinearModel(bandplant,vigband_ep, plotDia=plotDia, titleDia=titleDiaPlant, save_predic=save_predict, titleImg_p=titleImgPlant_p, save_reel=save_reel, titleImg_r=titleImgPlant_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCutPlant)
else:
    print("Data :")
    model = lm.UseLinearModel(MaskBand,bands2_e, plotDia=plotDia,titleDia=titleDia, save_predic=save_predict,titleImg_p=titleImg_p, save_reel=save_reel,titleImg_r=titleImg_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCut)

if save_model:
    if MaskPlant:
        jlib.dump(modelS, modelNameSand)
        jlib.dump(modelP, modelNamePlant)
    else :
        jlib.dump(model, modelName)
        
############ To make 3 models in same time all sand and plant #################
# """
# To do this cancel the code below, and comment the one above titled Machine Learning Model
# After running line 278 (else:) and tab down the 3 rows
# And MaskPlant = True (line 123)
# Now you can run the code
# """

# bandsand = Sand; bandplant = Plant; bando = MaskBands[2]
# bandsand = [bandsand[pos_bandz+1],bandsand[pos_bandz]]
# bandplant = [bandplant[pos_bandz+1],bandplant[pos_bandz]]
# if len(bando)!=0:
#     print("Attention bando with values")

# vigband_es = bandsand
# vigband_ep = bandplant

# MaskBand = [MaskBand[pos_bandz+1],MaskBand[pos_bandz]]
# bands2_e = MaskBand

# print("Data Sand :")
# modelS = lm.UseLinearModel(bandsand,vigband_es, plotDia=plotDia, titleDia=titleDiaSand, save_predic=save_predict, titleImg_p=titleImgSand_p, save_reel=save_reel, titleImg_r=titleImgSand_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCutSand)
# print("Data Plant :")
# modelP = lm.UseLinearModel(bandplant,vigband_ep, plotDia=plotDia, titleDia=titleDiaPlant, save_predic=save_predict, titleImg_p=titleImgPlant_p, save_reel=save_reel, titleImg_r=titleImgPlant_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCutPlant)
# print("Data :")
# model = lm.UseLinearModel(MaskBand,bands2_e, plotDia=plotDia,titleDia=titleDia, save_predic=save_predict,titleImg_p=titleImg_p, save_reel=save_reel,titleImg_r=titleImg_r, PlotCut = PlotCut, poscut=poscut, titleCut=titleCut)

# if save_model:
#     jlib.dump(modelS, modelNameSand)
#     jlib.dump(modelP, modelNamePlant)
#     jlib.dump(model, modelName)

############# Print indication #############

if minalti != 0:
    print(f'area study between {maxalti} and {minalti} meter of depth.')
else:
    print(f'area study {str}')
