# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 13:10:48 2022

@author: Antoine Cornu student of ENSG France
"""

###############################################################################
################# MAIN TO EVALUATE A Machine Learning MODEL ###################
################## TO CALCULATE BATHYMETRY BY AERIAL IMAGE ####################
###############################################################################
"""
Python file allows to use a model on data like worldview 3 
"""

############# Import and different python librairy need #############
"""
Python librairy you need:
numpy, joblib, scikit, gdal, imageio, matplotlib
"""

import numpy as np
import imageio as iio
import joblib as jlib
import Fonction.ProcessingData as pd
import Fonction.LowFilter as lf
import Fonction.LoadDataGdal as ldg
import Fonction.Masquing as mask
import Fonction.MachineLearningRegressor as mlr

############ Model & Image ##########
"""
Path to the model .sav and image
"""
#model path       
pathmodel = "Model_ML/ModelGB_allWV3.sav"
pathmodelsand = "Model_ML/ModelGB_allsandWV3.sav"
pathmodelplant = "Model_ML/ModelGB_allplantWV3.sav"
  
#Worldview 3 image
pathimg = "../Dataset/011098385010_01/011098385010_01_P001_MUL/18DEC21102146-M2AS_R1C1-011098385010_01_P001.tif"

pathbathy = "../Dataset/DEM/Bathymetric data of Ramla Bay.tif"

####### Parameter for bands image #######
"""
What band do you want to use to train the algorithm?
Bands: 
    Blue: B
    Green: G
    Red: R

ex: bandref = ['B', 'G', 'R']

nb_band: number of bands with bathymetry B, G, R, Pir, Z (exemple with data of Worldview 3)
"""
bandref = ['B','G','R']
nb_band = 5

####### Parameter to applicate a low filter on image data ######
"""
Application of a kernel filter on radiative data before making processing 
to calculate the TOA reflectance
"""
KernelFilter = True

####### Parameter for data mask #######
"""
There are several masks we apply to the data, True for application and False no application:   
    MaskWater: to separate water and land
    MaskPlant: to separate aquatic plant and sand with the band blue with a K-Means
    Usingkm: to use a model already existing
"""
MaskWater = True
MaskPlant = True

Usingkm = True
#model kmean
pathkm = "Model_KM/ModelKMeans.sav"

minDepth = 0
maxDepth = -10

######## Parameter #######
"""
This is the coordinate of a sand pixel,
it's to recognize sand pixels and plant pixels in the classification
"""
coor = [[1968,3750]] #WV3 all

######## Vignette ###########
#WV3
vignette = [1000,2200,2800,4096] ; str = 'all data'
# vignette = [1785,2042,3452,3816] ; str = 'average area'
# vignette = [1838,2110,3528,3711] ; str = 'little area' 

####### Save Prediction ##########
"""
SavePrediction: True to save the prediction
geoimage: True to save the prediction with projection in the GIS or just an image
and False without projection
The projection is the same as input data

3 variables for the names of the 3 prediction images to be recorded
"""
SavePrediction = False
geoimage = False
NameImgAll = 'Prediction/imgp_rf_allave_testall.tif'
NameImgSand = 'Prediction/imgp_rf_sandave_testall.tif'
NameImgPlant = 'Prediction/imgp_rf_plantave_testall.tif'

######## Parameter of differentes Plot for visualisation #########
"""
PlotCut is to plot two sections, one in the bathymetry in blue and another in 
the prediction in orange
"""
PlotCut = False
poscut = 1000
titleCut = 'Diagram_Cut'
titleCutSand = 'Diagram_Sand_Cut'
titleCutPlant = 'Diagram_Plant_Cut'

####### GDAL processing parameter #####
"""
useprocessingGDAL is a boolean
True to use gdal processing of images by GDAL on python, this process may not work, 
if it does not work, you must do the processing on QGIS before and put the variable in 
False and put the path to the processed image to the variable pathimgpro.

For the processing see the document "Guide QGIS processing data"
"""
useprocessingGDAL = False
pathimgpro = '../Dataset/Processing_Data/AllBandsWV3&Bathy.tif'

######### Evaluation Parameter #########
"""
SaveError: True pour sauvegarder la matrice d’erreur entre la bathymétrie 
prédicte et la bathymétrie réelle 
"""
SaveError = False
NameImgError = "difRP_rfave_010.tif"
NameImgErrorSand = "difRP_rfave_010sand.tif"
NameImgErrorPlant = "difRP_rfave_010plant.tif"

###############################################################################
############################ APPLICATION ######################################

###### Load model #######
model = jlib.load(pathmodel)
modelsand = jlib.load(pathmodelsand)
modelplant = jlib.load(pathmodelplant)

############################### Load images ###################################
if useprocessingGDAL:
    img = ldg.Download_Data(pathimg, pathbathy, JustBand, Data_sentinel2, DeletionBigBump, title)
    ldg.verif(img)
else:
    imggdal, img = ldg.Load_imggdalmultiband(pathimgpro)

############################### Application Low Filter ########################
if KernelFilter:
    imgarray = lf.PassBas(img, 4)

############################### Bands extraction ##############################
Bands = pd.Bands(imgarray)

############################### Bands masquing ################################

MaskBand = mask.MaskDepth(Bands, Bands[4], S=0, Depth=0)

# MaskBand = mask.MaskWater(Bands, Bands[3])

if Usingkm:
    modelkmean = jlib.load(pathkm)
    Sand, Plant = mask.UsingKMeans(modelkmean, MaskBand, coor)
else:
    Sand, Plant, O = mask.MaskPlantSand2(MaskBand, 0, coor)

if maxDepth != 0:
    MaskBand = mask.MaskDepth(MaskBand, MaskBand[4], S=minDepth, Depth=maxDepth)
    Sand = mask.MaskDepth(Sand, Sand[4], S=minDepth, Depth=maxDepth)
    Plant = mask.MaskDepth(Plant, Plant[4], S=minDepth, Depth=maxDepth)

for b in range(5):
    Sand[b] = Sand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
    Plant[b] = Plant[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]
    MaskBand[b] = MaskBand[b][vignette[0]:vignette[1],vignette[2]:vignette[3]]

############################# Application model ##############################

Datasetsand = mlr.Dataset(Sand[0:3])
Datasetplant = mlr.Dataset(Plant[0:3])
Datasetall = mlr.Dataset(MaskBand[0:3])

l,c = MaskBand[0].shape
info_ps = [Datasetsand[1],l,c]
info_pp = [Datasetplant[1],l,c]
info_predict = [Datasetall[1],l,c]

matmodel = mlr.Estimation(model, Datasetall[0], info_predict)
matmodelsand = mlr.Estimation(modelsand, Datasetsand[0], info_ps)
matmodelplant = mlr.Estimation(modelplant, Datasetplant[0], info_pp)

if SavePrediction:
    if geoimage:
        ldg.array2raster(imggdal, NameImgAll, matmodel)
        ldg.array2raster(imggdal, NameImgSand, matrfsand)
        ldg.array2raster(imggdal, NameImgPlant, matrfplant)
    else:    
        mlr.SavePredict(matmodel, NameImgAll)
        mlr.SavePredict(matrfsand, NameImgSand)
        mlr.SavePredict(matrfplant, NameImgPlant)

if PlotCut:
    mlr.CutPredict(matmodel, poscut, MaskBand[4], titleCut)
    mlr.CutPredict(matmodelsand, poscut, Sand[4], titleCutSand)
    mlr.CutPredict(matmodelplant, poscut, Plant[4], titleCutPlant)

# other apply for evaluation #

def Evaluation(matreel, matpredict, save=False, title="difimg.tif"):
    """
    

    Parameters
    ----------
    matreel : array
        Reel bathymetry.
    matpredict : array
        Predict Bathymetry.
    save : boolean, optional
        if you want to save the matrice of error. The default is False.
    title : string, optional
        name of the image, if save is true. The default is "difimg.tif".

    Returns
    -------
    None.

    """
    difmat = abs(matreel-matpredict)
    difmat2 = difmat**2
    mean = np.nanmean(difmat)
    mean2 = np.nanmean(difmat2)
    median = np.nanmedian(difmat)
    rmse = np.sqrt(mean2)
    print('RMSE :', rmse)
    print("Mean Absolute Error : ", mean)
    print("Median : ", median)
    
    if save:
        iio.imsave(title, difmat)

print("All data")
Evaluation(MaskBand[-1],matmodel,save=SaveError, title=NameImgError)
print(" ")
print("Sand data")
Evaluation(Sand[-1],matmodelsand,save=SaveError, title=NameImgErrorSand)
print(" ")
print("Plant data")
Evaluation(Plant[-1],matmodelplant,save=SaveError, title=NameImgErrorPlant)